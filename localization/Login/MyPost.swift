//
//  MyPost.swift
//  localization
//
//  Created by Developer on 5/14/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class MyPost: UIViewController , UITableViewDataSource , UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var button_addPost: UIButton!
    @IBOutlet weak var label_myPosts: UILabel!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var superView: UIView!
    var arrayMarket = NSMutableArray()
    @IBOutlet weak var label_sun: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.arrayMarket.removeAllObjects()
        self.fetchMyPostInteractedData()
        segmentedControl.addTarget(self, action: #selector(MyPost.segmentedControlValueChanged(segment:)), for:.valueChanged)
    }
    
    @objc func segmentedControlValueChanged(segment: UISegmentedControl) {
        
        
        
        if segment.selectedSegmentIndex == 0 {
            selectedIndex = 0
        }
        if segment.selectedSegmentIndex == 1 {
            selectedIndex = 1
        }
        if segment.selectedSegmentIndex == 2 {
            selectedIndex = 2
        }
      //  self.tableView.reloadData()
        
        if(selectedIndex == 0){
            
            self.arrayMarket.removeAllObjects()
            self.fetchMyPostInteractedData()
        }
        if(selectedIndex == 1){
            
            self.arrayMarket.removeAllObjects()
            self.fetchMyPostData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
    
    let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
    if(language == "English"){
    self.tableView.semanticContentAttribute = .forceLeftToRight
    self.superView.semanticContentAttribute = .forceLeftToRight
    self.upperView.semanticContentAttribute = .forceLeftToRight
    self.label_myPosts.text = ("My Posts".localizeStringUsingSystemLang)
        self.button_addPost.setTitle("Add".localizeStringUsingSystemLang, for: .normal)
  //  self.label_sun.text = ("SUN".localizeStringUsingSystemLang)
    }
    else if(language == "دری"){
    self.tableView.semanticContentAttribute = .forceRightToLeft
    self.superView.semanticContentAttribute = .forceRightToLeft
    self.upperView.semanticContentAttribute = .forceRightToLeft
    
    self.label_myPosts.text = ("My Posts".localizeString1(lang: "fa-AF"))
        self.button_addPost.setTitle("Add".localizeString1(lang: "fa-AF"), for: .normal)
   // self.label_sun.text = ("SUN".localizeString1(lang: "fa-AF"))
    }
    else {
    self.tableView.semanticContentAttribute = .forceRightToLeft
    self.superView.semanticContentAttribute = .forceRightToLeft
    self.upperView.semanticContentAttribute = .forceRightToLeft
    
    self.label_myPosts.text = ("My Posts".localizeString1(lang: "ps-AF"))
        self.button_addPost.setTitle("Add".localizeString1(lang: "ps-AF"), for: .normal)
   // self.label_sun.text = ("SUN".localizeString1(lang: "ps-AF"))
    }
    }
    
    func fetchMyPostData(){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            Alamofire.request("http://qtademo.com/saraf/api/post_list", method: .post , parameters: ["exm_id" : UserDefaults.standard.value(forKey: "exm_id")! , "uid" : UserDefaults.standard.value(forKey: "uid")! ], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        if( json["data"] as? NSDictionary != nil){
                            
                            if((json["data"] as! NSDictionary).value(forKey: "data") as? NSArray != nil){
                                self.arrayMarket = ((json["data"] as! NSDictionary).value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                                self.tableView.reloadData()
                            }
                            
                        }
                        else {
                            self.tableView.reloadData()
                        }
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func fetchMyPostInteractedData(){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            Alamofire.request("http://qtademo.com/saraf/api/user_other_posts_list", method: .post , parameters: ["uid" : UserDefaults.standard.value(forKey: "uid")! ], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        if( json["data"] as? NSDictionary != nil){
                            
                            if((json["data"] as! NSDictionary).value(forKey: "data") as? NSArray != nil){
                                self.arrayMarket = ((json["data"] as! NSDictionary).value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                                self.tableView.reloadData()
                            }
                            
                        }
                        else {
                            self.tableView.reloadData()
                        }
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMarket.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MyPostsCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MyPostsCell
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            cell.superView.semanticContentAttribute = .forceLeftToRight
            cell.label_name1.semanticContentAttribute = .forceLeftToRight
            cell.label_name.semanticContentAttribute = .forceLeftToRight
            cell.label_name.textAlignment = .left
            cell.label_name1.textAlignment = .left
            cell.label_to1.text = ("To".localizeStringUsingSystemLang)
            cell.label_sell.text = ("SELL".localizeStringUsingSystemLang)
            cell.label_sun.textAlignment  = .left
            cell.headerView.semanticContentAttribute = .forceLeftToRight
        }
        else if(language == "دری"){
            cell.superView.semanticContentAttribute = .forceRightToLeft
            cell.label_name.semanticContentAttribute = .forceRightToLeft
            cell.label_name1.semanticContentAttribute = .forceRightToLeft
            cell.label_to1.text = ("To".localizeString1(lang: "fa-AF"))
            cell.label_sell.text = ("SELL".localizeString1(lang: "fa-AF"))
            cell.label_name.textAlignment = .right
            cell.label_name1.textAlignment = .right
            cell.label_sun.textAlignment  = .right
            cell.headerView.semanticContentAttribute = .forceRightToLeft
        }
        else {
            cell.superView.semanticContentAttribute = .forceRightToLeft
            cell.label_name.semanticContentAttribute = .forceRightToLeft
            cell.label_name1.semanticContentAttribute = .forceRightToLeft
            cell.label_name.textAlignment = .right
            cell.label_name1.textAlignment = .right
            cell.label_sun.textAlignment  = .right
            
            cell.label_to1.text = ("To".localizeString1(lang: "ps-AF"))
            cell.label_sell.text = ("SELL".localizeString1(lang: "ps-AF"))
            cell.headerView.semanticContentAttribute = .forceRightToLeft
        }
        
        cell.label_name1.text = ((arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "fname") as! String) + " " +  ((arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "lname") as! String)
        
        cell.label_to.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "buy_cur") as? String
        cell.label_from.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "sell_cur") as? String
        
        let amount = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "amount") as! String
        cell.label_amount.text = String(describing: Double(amount)!.rounded(toPlaces: 2))
      //  cell.label_amount.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "amount") as? String
        //        cell.label_desc.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "comment") as? String
        
        let unixTimestamp = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "created_datetime") as! String
        let date = Date(timeIntervalSince1970: Double(unixTimestamp)!)
        
        let myString = ""
        cell.label_sun.text = (myString.getDay(date: date)) + " : " + (myString.getAfghanDate(date: date)) + " / " + (myString.getUSDate(date: date))
        cell.superView.layer.cornerRadius = 3.0
        let dateFormatter = DateFormatter()
        // dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "hh:mm aa" //Specify your format that you want
        cell.label_name.text = dateFormatter.string(from: date)
        
        //        cell.label_date.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "to_cur_code") as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 153.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(selectedIndex == 0){
            
            let regisetr : PostDetails = self.storyboard?.instantiateViewController(withIdentifier: "PostDetails") as! PostDetails
            regisetr.exp_id = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "exp_id") as! String
            self.navigationController?.pushViewController(regisetr, animated: true)
        }
        
    }
    
    
    
    
    @IBAction func action_addPost(_ sender: Any) {
        
        let regisetr : AddPost = self.storyboard?.instantiateViewController(withIdentifier: "AddPost") as! AddPost
        
        self.navigationController?.pushViewController(regisetr, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

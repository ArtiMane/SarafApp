//
//  RateVC.swift
//  localization
//
//  Created by Developer on 5/13/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class RateVC: UIViewController , UITableViewDelegate , UITableViewDataSource{

    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var label_sun: UILabel!
    @IBOutlet weak var label_rates: UILabel!
    
    
    @IBOutlet weak var label_usDate: UILabel!
    @IBOutlet weak var label_afghanDate: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var segmnetedControl1  = UISegmentedControl()
    var exm_name = ""
    var exmidArray = NSMutableArray()
    var arrayMarket = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
      //  segmentedControl.apportionsSegmentWidthsByContent = true
        
    self.segmnetedControl1.frame = CGRect(x: 8, y: 8, width: 0, height: 40)
    self.segmnetedControl1.tintColor = UIColor(displayP3Red: 0/255, green: 209/255, blue: 174/255, alpha: 1.0)
    self.segmnetedControl1.addTarget(self, action: #selector(RateVC.segmentedSelected(sender:)), for: .valueChanged)
    self.myScrollView.addSubview(segmnetedControl1)
        
    
        let date = Date()
        let mystring = ""
        self.label_afghanDate.text = mystring.getAfghanDate(date: date)
        self.label_sun.text = mystring.getDay(date: date)
        self.label_usDate.text = mystring.getUSDate(date: date)
        self.callExchangeMarketList()
        
    
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            self.tableView.semanticContentAttribute = .forceLeftToRight
            self.upperView.semanticContentAttribute = .forceLeftToRight
            self.label_rates.text = ("Rates".localizeStringUsingSystemLang)
         //   self.label_sun.text = ("SUN".localizeStringUsingSystemLang)
        }
        else if(language == "دری"){
            self.tableView.semanticContentAttribute = .forceRightToLeft
            self.upperView.semanticContentAttribute = .forceRightToLeft
            
            self.label_rates.text = ("Rates".localizeString1(lang: "fa-AF"))
          //  self.label_sun.text = ("SUN".localizeString1(lang: "fa-AF"))
        }
        else {
            self.tableView.semanticContentAttribute = .forceRightToLeft
            self.upperView.semanticContentAttribute = .forceRightToLeft
            
            self.label_rates.text = ("Rates".localizeString1(lang: "ps-AF"))
           // self.label_sun.text = ("SUN".localizeString1(lang: "ps-AF"))
        }
    }
    
    func callExchangeMarketList(){
        
        let usertype = UserDefaults.standard.value(forKey: "LoginType") as! String
        var agent = ""
        if(usertype == "user"){
            agent  = "0"
        }
        else {
            agent = "1"
        }
        
        print("agent value = \(agent)")
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
        //    loadingNotification.label.text = "Registering"
            let auth_token = UserDefaults.standard.value(forKey: "auth_token") as! String
            print(auth_token)
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]

            Alamofire.request("http://qtademo.com/saraf/api/exchange_market_list", method: .post , parameters: ["agent" : agent], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        let data1 = json["data"] as! NSArray
                        let exm_id = UserDefaults.standard.value(forKey: "exm_id") as! String
                        
                        let counnt = data1.count
                        
                        for var i in 0 ..< data1.count {
                            
                            let exm_id1 = (data1[i] as! NSDictionary).value(forKey: "exm_id") as! String
                            var exm_name = ""
                            let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
                            if(language == "English"){
                                exm_name =  (data1[i] as! NSDictionary).value(forKey: "exm_name") as! String
                            }
                            else if(language == "دری"){
                                exm_name =  (data1[i] as! NSDictionary).value(forKey: "exm_dari_name") as! String
                            }
                            else {
                                exm_name =  (data1[i] as! NSDictionary).value(forKey: "exm_pashtu_name") as! String
                            }
                           // let exm_name = (data1[i] as! NSDictionary).value(forKey: "exm_name") as! String
                            self.exmidArray.add(exm_id1)
                            self.segmnetedControl1.insertSegment(withTitle: exm_name, at: i, animated: true)
                            self.segmnetedControl1.apportionsSegmentWidthsByContent = true
                            self.segmnetedControl1.setWidth(200, forSegmentAt: i)
                            
                        }
                        self.segmnetedControl1.apportionsSegmentWidthsByContent = true
                        
                        let totalwidth = counnt * 200
                        self.segmnetedControl1.frame = CGRect(x: self.segmnetedControl1.frame.origin.x, y: self.segmnetedControl1.frame.origin.y, width: CGFloat(totalwidth), height: self.segmnetedControl1.frame.size.height)
                        self.myScrollView.contentSize = CGSize(width: self.segmnetedControl1.frame.size.width + 8, height: self.myScrollView.frame.size.height)
                       self.segmnetedControl1.selectedSegmentIndex = 0
                        self.callMarketListExm(exmId: self.exmidArray.object(at: 0) as! String)
                        
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    
    }
    
    @objc func segmentedSelected(sender : UISegmentedControl){
        
        let selected = sender.selectedSegmentIndex
        self.arrayMarket.removeAllObjects()
        self.tableView.reloadData()
        self.callMarketListExm(exmId: exmidArray.object(at: selected) as! String)
        
        
        
    }
    
    func callMarketListExm(exmId : String){
        
        
        let usertype = UserDefaults.standard.value(forKey: "LoginType") as! String
        var agent = ""
        if(usertype == "user"){
            agent  = "0"
        }
        else {
            agent = "1"
        }
        
        print("agent value = \(agent)")
        
        self.arrayMarket.removeAllObjects()
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            Alamofire.request("http://qtademo.com/saraf/api/exm_rate", method: .post , parameters: ["exm_id" : exmId , "agent" : agent], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int

                    if(code == 200){
                        self.arrayMarket = (json["data"] as! NSArray).mutableCopy() as! NSMutableArray
                        self.tableView.reloadData()
                    }
                    else {
                        self.tableView.reloadData()
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMarket.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RateCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! RateCell
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        
        if(UserDefaults.standard.value(forKey: "LoginType") as! String == "user"){
            
            cell.button_update.isHidden = true
        }
        else {
            cell.button_update.isHidden = false
        }
        
        if(language == "English"){
            cell.superView.semanticContentAttribute = .forceLeftToRight
            cell.label_buy.text = ("BUY".localizeStringUsingSystemLang)
            cell.label_sell.text = ("SELL".localizeStringUsingSystemLang)
            cell.button_update.setTitle("Update".localizeStringUsingSystemLang, for: .normal)
            cell.button_history.setTitle("History".localizeStringUsingSystemLang, for: .normal)
        }
        else if(language == "دری"){
            cell.superView.semanticContentAttribute = .forceRightToLeft
            cell.label_buy.text = ("BUY".localizeString1(lang: "fa-AF"))
            cell.label_sell.text = ("SELL".localizeString1(lang: "fa-AF"))
            cell.button_update.setTitle("Update".localizeString1(lang: "fa-AF"), for: .normal)
            cell.button_history.setTitle("History".localizeString1(lang: "fa-AF"), for: .normal)
            
        }
        else {
            cell.superView.semanticContentAttribute = .forceRightToLeft
            cell.label_buy.text = ("BUY".localizeString1(lang: "ps-AF"))
            cell.label_sell.text = ("SELL".localizeString1(lang: "ps-AF"))
            cell.button_update.setTitle("Update".localizeString1(lang: "ps-AF"), for: .normal)
            cell.button_history.setTitle("History".localizeString1(lang: "ps-AF"), for: .normal)
        }
        let buyValue = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "buy_rate") as! String
         cell.label_buyValue.text =  String(describing: Double(buyValue)!.rounded(toPlaces: 2))
        
        let sellValue = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "sell_rate") as! String
        cell.label_sellValue.text =  String(describing: Double(sellValue)!.rounded(toPlaces: 2))
        cell.label_from.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "from_cur_code") as? String
         cell.label_to.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "to_cur_code") as? String
        
        cell.button_history.addTarget(self, action: #selector(RateVC.showHistory(sender:)), for: .touchUpInside)
        cell.button_history.layer.cornerRadius = 3.0
        cell.button_update.layer.cornerRadius = 3.0
        cell.button_update.addTarget(self, action: #selector(RateVC.update(sender:)), for: .touchUpInside)
        cell.superView.layer.cornerRadius  = 3.0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 81.0
    }
    
    @objc func showHistory(sender : UIButton){
        
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let exm_id = (arrayMarket.object(at: (indexPath?.row)!) as! NSDictionary).value(forKey: "exmr_id") as! String
        let regisetr : HistoryVC = self.storyboard?.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryVC
        regisetr.exm_i = exm_id as String
        regisetr.to = (arrayMarket.object(at: (indexPath?.row)!) as! NSDictionary).value(forKey: "to_cur_code") as! String
        regisetr.from = (arrayMarket.object(at: (indexPath?.row)!) as! NSDictionary).value(forKey: "from_cur_code") as! String
        
        self.navigationController?.pushViewController(regisetr, animated: true)
        
        
    }
    
    @objc func update(sender : UIButton){
        
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
       
        let regisetr : UpdateRate = self.storyboard?.instantiateViewController(withIdentifier: "UpdateRate") as! UpdateRate
        
        regisetr.buyrate = (arrayMarket.object(at: (indexPath?.row)!) as! NSDictionary).value(forKey: "buy_rate") as! String
        regisetr.sellrate = (arrayMarket.object(at: (indexPath?.row)!) as! NSDictionary).value(forKey: "sell_rate") as! String
        regisetr.tocurrency = (arrayMarket.object(at: (indexPath?.row)!) as! NSDictionary).value(forKey: "to_cur_code") as! String
        regisetr.fromcurrency = (arrayMarket.object(at: (indexPath?.row)!) as! NSDictionary).value(forKey: "from_cur_code") as! String
        regisetr.exmr_id = (arrayMarket.object(at: (indexPath?.row)!) as! NSDictionary).value(forKey: "exmr_id") as! String
        
        self.navigationController?.pushViewController(regisetr, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    
    func getAfghanDate(date: Date) -> String{
        
       
        let dateFormatter1 : DateFormatter = DateFormatter()
        dateFormatter1.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.persian)! as Calendar
        dateFormatter1.dateFormat = "MM-dd-yyyy"
        let newDate = dateFormatter1.string(from: date)
        return newDate
    }
    
    func getDay(date: Date) -> String {
        
        var newDate = ""
        let dateFormatter2 : DateFormatter = DateFormatter()
        dateFormatter2.dateFormat = "EEEE"
        let today = dateFormatter2.string(from: date)
        
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English")
        {
            if(today == "Monday"){
                
                newDate = "Monday".localizeStringUsingSystemLang
                
            }
            else if(today == "Wednesday"){
                
                newDate = "Wednesday".localizeStringUsingSystemLang
            }
            else if(today == "Tuesday"){
                newDate = "Tuesday".localizeStringUsingSystemLang
            }
            else if(today == "Thursday"){
                newDate = "Thursday".localizeStringUsingSystemLang
            }
            else if(today == "Friday"){
                newDate = "Friday".localizeStringUsingSystemLang
            }
            else if(today == "Saturday"){
                newDate = "Saturday".localizeStringUsingSystemLang
            }
            else if(today == "Sunday"){
                newDate = "Sunday".localizeStringUsingSystemLang
            }
        }
        else if(language == "دری"){
            if(today == "Monday"){
                
                newDate = "Monday".localizeString1(lang: "fa-AF")
                
            }
            else if(today == "Wednesday"){
                
                newDate = "Wednesday".localizeString1(lang: "fa-AF")
            }
            else if(today == "Tuesday"){
                newDate = "Tuesday".localizeString1(lang: "fa-AF")
            }
            else if(today == "Thursday"){
                newDate = "Thursday".localizeString1(lang: "fa-AF")
            }
            else if(today == "Friday"){
                newDate = "Friday".localizeString1(lang: "fa-AF")
            }
            else if(today == "Saturday"){
                newDate = "Saturday".localizeString1(lang: "fa-AF")
            }
            else if(today == "Sunday"){
                newDate = "Sunday".localizeString1(lang: "fa-AF")
            }
        }
        else {
            if(today == "Monday"){
                
                newDate = "Monday".localizeString1(lang: "ps-AF")
                
            }
            else if(today == "Wednesday"){
                
                newDate = "Wednesday".localizeString1(lang: "ps-AF")
            }
            else if(today == "Tuesday"){
                newDate = "Tuesday".localizeString1(lang: "ps-AF")
            }
            else if(today == "Thursday"){
                newDate = "Thursday".localizeString1(lang: "ps-AF")
            }
            else if(today == "Friday"){
                newDate = "Friday".localizeString1(lang: "ps-AF")
            }
            else if(today == "Saturday"){
                newDate = "Saturday".localizeString1(lang: "ps-AF")
            }
            else if(today == "Sunday"){
                newDate = "Sunday".localizeString1(lang: "ps-AF")
            }
        }
        
        
        return newDate
    }
    
    func getUSDate( date : Date) -> String
    {
        let dateFormatter1 : DateFormatter = DateFormatter()
        dateFormatter1.dateFormat = "MM-dd-yyyy"
        let newDate = dateFormatter1.string(from: date)
        return newDate
    }
}

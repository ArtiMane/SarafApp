//
//  RateCell.swift
//  localization
//
//  Created by Developer on 5/13/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class RateCell: UITableViewCell {

    @IBOutlet weak var label_date: UILabel!
    @IBOutlet weak var label_sellValue: UILabel!
    @IBOutlet weak var label_buyValue: UILabel!
    @IBOutlet weak var label_sell: UILabel!
    @IBOutlet weak var label_buy: UILabel!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var label_from: UILabel!
    
    @IBOutlet weak var button_history: UIButton!
    @IBOutlet weak var button_update: UIButton!
    @IBOutlet weak var label_to: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

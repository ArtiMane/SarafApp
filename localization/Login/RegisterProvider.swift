//
//  RegisterProvider.swift
//  localization
//
//  Created by Developer on 4/30/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class RegisterProvider: UIViewController  , UIPickerViewDataSource , UIPickerViewDelegate {

    @IBOutlet weak var viewTitle: UIView!
    
    @IBOutlet weak var viewFileAttachment: UIView!
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var viewLicenceExpiryDate: UIView!
    @IBOutlet weak var viewLicenceNumber: UIView!
    @IBOutlet weak var viewPhoneNumber: UIView!
    @IBOutlet weak var viewMobileNumber: UIView!
    @IBOutlet weak var viewOfficeNumber: UIView!
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var viewMarket: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewNameRegisteredInLicence: UIView!
    @IBOutlet weak var viewSarafiName: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var viewId: UIView!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var viewRegister: UIView!
    
    @IBOutlet weak var imageId: UIImageView!
    @IBOutlet weak var imageFirstName: NSLayoutConstraint!
    
    @IBOutlet weak var imageFirstName1: UIImageView!
    
    @IBOutlet weak var imageLastName: UIImageView!
    
    @IBOutlet weak var imageMobileNumber: UIImageView!
    @IBOutlet weak var imageCountry: UIImageView!
    @IBOutlet weak var imageStore: UIImageView!
    @IBOutlet weak var imageCity: UIImageView!
    @IBOutlet weak var imageNameRegistered: UIImageView!
    @IBOutlet weak var imageSarafiName: UIImageView!
    
    
    @IBOutlet weak var textId: UITextField!
    
    @IBOutlet weak var textFirstName: UITextField!
    
    @IBOutlet weak var textNameRegistered: UITextField!
    @IBOutlet weak var textSarafiName: UITextField!
    @IBOutlet weak var textLastName: UITextField!
    
    @IBOutlet weak var textAttachment: UITextField!
    @IBOutlet weak var textType: UITextField!
    @IBOutlet weak var textLicenceExpiryNumber: UITextField!
    @IBOutlet weak var textLicenceNumber: UITextField!
    @IBOutlet weak var textPhoneNumber: UITextField!
    @IBOutlet weak var textMobileNumber: UITextField!
    @IBOutlet weak var textOfficeNumber: UITextField!
    @IBOutlet weak var textCountry: UITextField!
    @IBOutlet weak var textMarket: UITextField!
    @IBOutlet weak var textCity: UITextField!
    
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var buttonRegister: UIButton!
    
    @IBOutlet weak var textRegister: UITextField!
    var arrayUserType = [String]()
    var pickerView = UIPickerView()
    var selectedIndex = 0
    var buttonRegister1 = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        arrayUserType = ["Admin","Agent","Money Transfer Service Provider"]
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        textRegister.inputView = pickerView
        
        textId.attributedPlaceholder = NSAttributedString(string: "Id",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textFirstName.attributedPlaceholder = NSAttributedString(string: "First Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textLastName.attributedPlaceholder = NSAttributedString(string: "Last Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textSarafiName.attributedPlaceholder = NSAttributedString(string: "Sarafi Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textNameRegistered.attributedPlaceholder = NSAttributedString(string: "Name registered in license",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textCity.attributedPlaceholder = NSAttributedString(string: "City",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textMarket.attributedPlaceholder = NSAttributedString(string: "Market Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textCountry.attributedPlaceholder = NSAttributedString(string: "Country",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textCity.attributedPlaceholder = NSAttributedString(string: "City",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textOfficeNumber.attributedPlaceholder = NSAttributedString(string: "Office Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textMobileNumber.attributedPlaceholder = NSAttributedString(string: "Mobile Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textPhoneNumber.attributedPlaceholder = NSAttributedString(string: "Phone Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textLicenceNumber.attributedPlaceholder = NSAttributedString(string: "License Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textLicenceExpiryNumber.attributedPlaceholder = NSAttributedString(string: "License Expiry Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textType.attributedPlaceholder = NSAttributedString(string: "Type",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textAttachment.attributedPlaceholder = NSAttributedString(string: "Attachment",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        
        viewSarafiName.layer.cornerRadius = 3.0
        viewId.layer.cornerRadius = 3.0
        viewFileAttachment.layer.cornerRadius = 3.0
        viewFirstName.layer.cornerRadius = 3.0
        viewLastName.layer.cornerRadius = 3.0
        viewLicenceExpiryDate.layer.cornerRadius = 3.0
        viewNameRegisteredInLicence.layer.cornerRadius = 3.0
        viewLicenceNumber.layer.cornerRadius = 3.0
        viewPhoneNumber.layer.cornerRadius = 3.0
        viewOfficeNumber.layer.cornerRadius = 3.0
        viewMobileNumber.layer.cornerRadius = 3.0
        viewType.layer.cornerRadius = 3.0
        viewCity.layer.cornerRadius = 3.0
        viewCountry.layer.cornerRadius = 3.0
        viewMarket.layer.cornerRadius = 3.0
        buttonRegister1.layer.cornerRadius = 3.0
        viewRegister.layer.cornerRadius = 3.0
        
        buttonRegister1.frame = CGRect(x: 16, y: 16, width: self.view.frame.size.width - 32, height: 50)
        buttonRegister1.setTitle("Register", for: .normal)
        buttonRegister1.setTitleColor(UIColor.black, for: .normal)
        buttonRegister1.layer.cornerRadius = 3.0
        buttonRegister1.backgroundColor = UIColor(displayP3Red: 0/255, green: 209/255, blue: 174/255, alpha: 1.0)
        buttonRegister1.titleLabel?.font = UIFont.boldSystemFont(ofSize: 19.0)
        buttonRegister1.isHidden = true
        self.myScrollView.addSubview(buttonRegister1)
        setViewAccordingToIndex(row: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(UserDefaults.standard.value(forKey: "selectedLanguage") as! String != "English"){
            
            self.viewId.semanticContentAttribute = .forceRightToLeft
            self.viewFileAttachment.semanticContentAttribute = .forceRightToLeft
            self.viewRegister.semanticContentAttribute = .forceRightToLeft
            self.viewCountry.semanticContentAttribute = .forceRightToLeft
            self.viewMarket.semanticContentAttribute = .forceRightToLeft
            self.viewMobileNumber.semanticContentAttribute = .forceRightToLeft
            self.viewCity.semanticContentAttribute = .forceRightToLeft
            self.viewType.semanticContentAttribute = .forceRightToLeft
            self.viewTitle.semanticContentAttribute = .forceRightToLeft
            self.viewLastName.semanticContentAttribute = .forceRightToLeft
            self.viewFirstName.semanticContentAttribute = .forceRightToLeft
            self.viewLastName.semanticContentAttribute = .forceRightToLeft
            self.viewSarafiName.semanticContentAttribute = .forceRightToLeft
            self.viewPhoneNumber.semanticContentAttribute = .forceRightToLeft
            self.viewOfficeNumber.semanticContentAttribute = .forceRightToLeft
            self.viewLicenceExpiryDate.semanticContentAttribute = .forceRightToLeft
            self.viewNameRegisteredInLicence.semanticContentAttribute = .forceRightToLeft
            self.viewLicenceNumber.semanticContentAttribute = .forceRightToLeft
            
            
            self.textRegister.textAlignment  = .center
            self.textId.textAlignment  = .right
            self.textMobileNumber.textAlignment  = .right
            self.textFirstName.textAlignment  = .right
            self.textLastName.textAlignment  = .right
            self.textSarafiName.textAlignment  = .right
            self.textNameRegistered.textAlignment  = .right
            self.textCity.textAlignment  = .right
            self.textMarket.textAlignment  = .right
            self.textCountry.textAlignment  = .right
            self.textOfficeNumber.textAlignment  = .right
            self.textLicenceNumber.textAlignment  = .right
            self.textPhoneNumber.textAlignment  = .right
            self.textLicenceExpiryNumber.textAlignment  = .right
            self.textType.textAlignment  = .right
            self.textAttachment.textAlignment  = .right
            
            self.buttonBack.setImage(UIImage(named:"rightArrow.png"), for: .normal)
        }
    }
    
    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLayoutSubviews() {
        
        myScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1200)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayUserType.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayUserType[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textRegister.text = arrayUserType[row]
        selectedIndex = row
        self.setViewAccordingToIndex(row: selectedIndex)
        
        //self.view.endEditing(true)
    }
    
    func setViewAccordingToIndex(row : Int){
        
        switch row {
            
        case 0 :
            
            viewCountry.isHidden = true
            viewMobileNumber.isHidden = true
            viewOfficeNumber.isHidden = true
            viewPhoneNumber.isHidden = true
            viewLicenceNumber.isHidden = true
            viewLicenceExpiryDate.isHidden = true
            viewType.isHidden = true
            viewFileAttachment.isHidden = true
            
            imageId.image = UIImage(named: "person.png")
            imageFirstName1.image = UIImage(named: "person.png")
            imageLastName.image = UIImage(named: "type.png")
            imageSarafiName.image = UIImage(named: "person.png")
            imageNameRegistered.image = UIImage(named: "Password.png")
            imageCity.image = UIImage(named: "email.png")
            imageStore.image = UIImage(named: "mobileaNumber.png")
            
            textId.attributedPlaceholder = NSAttributedString(string: "First Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textFirstName.attributedPlaceholder = NSAttributedString(string: "Last Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textLastName.attributedPlaceholder = NSAttributedString(string: "Type",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textSarafiName.attributedPlaceholder = NSAttributedString(string: "Username",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textNameRegistered.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textCity.attributedPlaceholder = NSAttributedString(string: "Email Id",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textMarket.attributedPlaceholder = NSAttributedString(string: "Mobile Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            buttonRegister1.isHidden = false
            buttonRegister1.frame = CGRect(x: buttonRegister1.frame.origin.x, y: viewMarket.frame.origin.y + viewMarket.frame.size.height + 50, width: buttonRegister1.frame.size.width, height: buttonRegister1.frame.size.height)
            
            break
            
        case 1 :
            
            viewCountry.isHidden = false
            viewMobileNumber.isHidden = true
            viewOfficeNumber.isHidden = true
            viewPhoneNumber.isHidden = true
            viewLicenceNumber.isHidden = true
            viewLicenceExpiryDate.isHidden = true
            viewType.isHidden = true
            viewFileAttachment.isHidden = true
            
            
            imageId.image = UIImage(named: "person.png")
            imageFirstName1.image = UIImage(named: "person.png")
            imageLastName.image = UIImage(named: "type.png")
            imageSarafiName.image = UIImage(named: "person.png")
            imageNameRegistered.image = UIImage(named: "Password.png")
            imageCity.image = UIImage(named: "email.png")
            imageStore.image = UIImage(named: "mobileaNumber.png")
            imageCountry.image = UIImage(named: "store.png")
            
            textId.attributedPlaceholder = NSAttributedString(string: "First Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textFirstName.attributedPlaceholder = NSAttributedString(string: "Last Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textLastName.attributedPlaceholder = NSAttributedString(string: "Type",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textSarafiName.attributedPlaceholder = NSAttributedString(string: "Username",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textNameRegistered.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textCity.attributedPlaceholder = NSAttributedString(string: "Email Id",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textMarket.attributedPlaceholder = NSAttributedString(string: "Mobile Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textCountry.attributedPlaceholder = NSAttributedString(string: "Branch Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            
            buttonRegister1.isHidden = false
            buttonRegister1.frame = CGRect(x: buttonRegister1.frame.origin.x, y: viewCountry.frame.origin.y + viewCountry.frame.size.height + 50, width: buttonRegister1.frame.size.width, height: buttonRegister1.frame.size.height)
            
            break
            
        case 2 :
            
            viewCountry.isHidden = false
            viewMobileNumber.isHidden = false
            viewOfficeNumber.isHidden = false
            viewPhoneNumber.isHidden = false
            viewLicenceNumber.isHidden = false
            viewLicenceExpiryDate.isHidden = false
            viewType.isHidden = false
            viewFileAttachment.isHidden = false
            
            imageId.image = UIImage(named: "id.png")
            imageFirstName1.image = UIImage(named: "person.png")
            imageLastName.image = UIImage(named: "person.png")
            imageSarafiName.image = UIImage(named: "person.png")
            imageNameRegistered.image = UIImage(named: "person.png")
            imageCity.image = UIImage(named: "city.png")
            imageStore.image = UIImage(named: "store.png")
            
            textId.attributedPlaceholder = NSAttributedString(string: "Id",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textFirstName.attributedPlaceholder = NSAttributedString(string: "First Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textLastName.attributedPlaceholder = NSAttributedString(string: "Last Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textSarafiName.attributedPlaceholder = NSAttributedString(string: "Sarafi Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textNameRegistered.attributedPlaceholder = NSAttributedString(string: "Name registered in license",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textCity.attributedPlaceholder = NSAttributedString(string: "City",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textMarket.attributedPlaceholder = NSAttributedString(string: "Market Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textCountry.attributedPlaceholder = NSAttributedString(string: "Country",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textCity.attributedPlaceholder = NSAttributedString(string: "City",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textOfficeNumber.attributedPlaceholder = NSAttributedString(string: "Office Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textMobileNumber.attributedPlaceholder = NSAttributedString(string: "Mobile Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textPhoneNumber.attributedPlaceholder = NSAttributedString(string: "Phone Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textLicenceNumber.attributedPlaceholder = NSAttributedString(string: "License Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textLicenceExpiryNumber.attributedPlaceholder = NSAttributedString(string: "License Expiry Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textType.attributedPlaceholder = NSAttributedString(string: "Type",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            textAttachment.attributedPlaceholder = NSAttributedString(string: "Attachment",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            buttonRegister1.isHidden = false
            buttonRegister1.frame = CGRect(x: buttonRegister1.frame.origin.x, y: viewFileAttachment.frame.origin.y + viewFileAttachment.frame.size.height + 50, width: buttonRegister1.frame.size.width, height: buttonRegister1.frame.size.height)
            
            break
        default:
            break
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

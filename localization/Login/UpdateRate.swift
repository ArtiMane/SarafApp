//
//  UpdateRate.swift
//  localization
//
//  Created by Developer on 5/18/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class UpdateRate: UIViewController {

    
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var buttonUpdate: UIButton!
    @IBOutlet weak var text_sellRate: UITextField!
    @IBOutlet weak var text_buyRate: UITextField!
    @IBOutlet weak var label_toCurrency1: UILabel!
    @IBOutlet weak var label_fromCurrency1: UILabel!
    @IBOutlet weak var view_buyRate: UIView!
    @IBOutlet weak var view_sellRate: UIView!
    @IBOutlet weak var view_toCurrency: UIView!
    @IBOutlet weak var view_fromCurrency: UIView!
    @IBOutlet weak var label_sellRate: UILabel!
    @IBOutlet weak var label_buyRate: UILabel!
    @IBOutlet weak var label_toCurrency: UILabel!
    @IBOutlet weak var label_fromCurrency: UILabel!
    @IBOutlet weak var currencyView: UIView!
    @IBOutlet weak var label_name: UILabel!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var label_updateRate: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var superView: UIView!
    
    var sellrate = ""
    var buyrate = ""
    var tocurrency = ""
    var fromcurrency = ""
    var exmr_id = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view_buyRate.layer.cornerRadius = 3.0
        self.view_sellRate.layer.cornerRadius = 3.0
        self.view_toCurrency.layer.cornerRadius = 3.0
        self.view_fromCurrency.layer.cornerRadius = 3.0
        self.buttonUpdate.layer.cornerRadius = 3.0
        
        self.label_fromCurrency1.text = fromcurrency
        self.label_toCurrency1.text = tocurrency
        self.text_buyRate.text = buyrate
        self.text_sellRate.text = sellrate
        
        self.label_name.text = (UserDefaults.standard.value(forKey: "fname") as! String) + " " + (UserDefaults.standard.value(forKey: "lname") as! String)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            self.headerView.semanticContentAttribute = .forceLeftToRight
            self.superView.semanticContentAttribute = .forceLeftToRight
            self.currencyView.semanticContentAttribute = .forceLeftToRight
            
            self.label_name.textAlignment = .left
            
             self.label_updateRate.text = ("Update Rate".localizeStringUsingSystemLang)
            self.label_toCurrency.text = ("To Currency".localizeStringUsingSystemLang)
            self.label_fromCurrency.text = ("From Currency".localizeStringUsingSystemLang)
            self.label_buyRate.text = ("Buy Rate".localizeStringUsingSystemLang)
            self.label_fromCurrency.text = ("Sell Rate".localizeStringUsingSystemLang)
            
            self.buttonBack.setImage(UIImage(named : "leftArrow.png"), for: .normal)
            
            self.buttonUpdate.setTitle("Update".localizeStringUsingSystemLang, for: .normal)
            
        }
        else if(language == "دری"){
            self.headerView.semanticContentAttribute = .forceRightToLeft
            self.superView.semanticContentAttribute = .forceRightToLeft
            self.currencyView.semanticContentAttribute = .forceRightToLeft
            
            self.label_name.textAlignment = .right
            
            self.label_updateRate.text = ("Update Rate".localizeString1(lang: "fa-AF"))
            self.label_toCurrency.text = ("To Currency".localizeString1(lang: "fa-AF"))
            self.label_fromCurrency.text = ("From Currency".localizeString1(lang: "fa-AF"))
            self.label_buyRate.text = ("Buy Rate".localizeString1(lang: "fa-AF"))
            self.label_fromCurrency.text = ("Sell Rate".localizeString1(lang: "fa-AF"))
            
            self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
            
            self.buttonUpdate.setTitle("Update".localizeString1(lang: "fa-AF"), for: .normal)
            
            
        }
        else {
            self.headerView.semanticContentAttribute = .forceRightToLeft
            self.superView.semanticContentAttribute = .forceRightToLeft
            self.currencyView.semanticContentAttribute = .forceRightToLeft
            
            self.label_name.textAlignment = .right
            
            self.label_updateRate.text = ("Update Rate".localizeString1(lang: "ps-AF"))
            self.label_toCurrency.text = ("To Currency".localizeString1(lang: "ps-AF"))
            self.label_fromCurrency.text = ("From Currency".localizeString1(lang: "ps-AF"))
            self.label_buyRate.text = ("Buy Rate".localizeString1(lang: "ps-AF"))
            self.label_fromCurrency.text = ("Sell Rate".localizeString1(lang: "ps-AF"))
            
            self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
            
            self.buttonUpdate.setTitle("Update".localizeString1(lang: "ps-AF"), for: .normal)
            
            
        }
    }
    
    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func action_update(_ sender: Any) {
        
        if(self.text_sellRate.text == "" && self.text_buyRate.text == ""){
            
            self.image1.isHidden = false
            self.image2.isHidden = false
        }
        else if(self.text_buyRate.text == "" ){
            
            self.image1.isHidden = false
        }
        else if(self.text_sellRate.text == "" ){
            
            self.image2.isHidden = false
        }
        else {
            
            if CheckConnection.isConnectedToNetwork() {
                print("Connected")
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = .indeterminate
                //    loadingNotification.label.text = "Registering"
                
                let headers : HTTPHeaders = [
                    "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
                ]
                
                Alamofire.request("http://qtademo.com/saraf/api/update_rates", method: .post , parameters: ["exmr_id" : exmr_id , "br" : self.text_buyRate.text! ,"sr" : text_sellRate.text! ,"aid" : UserDefaults.standard.value(forKey: "uid")! ], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    switch response.result {
                        
                        
                    case .success:
                        
                        
                        print(response)
                        
                        let json : [String : Any] = response.result.value as! [String : Any]
                        let code :Int = json["code"] as! Int
                        
                        if(code == 200){
                            
                            UserDefaults.standard.set("1", forKey: "ValueUpdated")
                            
                            let alert : UIAlertController = UIAlertController(title: "", message: "Values updated successfully", preferredStyle: .actionSheet)
                            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                                alert.dismiss(animated: true, completion: nil)
                            })
                            alert.addAction(actionSheet)
                            self.present(alert, animated: true, completion: nil)
                        }
                        else {
                            let alert : UIAlertController = UIAlertController(title: "Error", message: "Error occured while updating values.", preferredStyle: .actionSheet)
                            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                                alert.dismiss(animated: true, completion: nil)
                            })
                            alert.addAction(actionSheet)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        break
                    case .failure(let error):
                        
                        print(error)
                        let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(actionSheet)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                })
                
            }
            else{
                let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
                let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                })
                alert.addAction(actionSheet)
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

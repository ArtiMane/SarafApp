//
//  Profile.swift
//  localization
//
//  Created by Developer on 5/14/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class Profile: UIViewController {

    @IBOutlet weak var buttonLogout: UIButton!
    @IBOutlet weak var label_accoutnDetails: UILabel!
    @IBOutlet weak var label_personalDetaisl: UILabel!
    @IBOutlet weak var label_myProfile: UILabel!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var label_licenseExpiryDate: UILabel!
    @IBOutlet weak var label_offNo: UILabel!
    @IBOutlet weak var label_name: UILabel!
    
    @IBOutlet weak var label_licenseNo: UILabel!
    @IBOutlet weak var label_sarafiName: UILabel!
    @IBOutlet weak var label_email: UILabel!
    
    @IBOutlet weak var label_mob: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getUserInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            
            superView.semanticContentAttribute = .forceLeftToRight
            self.label_licenseExpiryDate.textAlignment = .left
            self.label_licenseNo.textAlignment = .left
            self.label_offNo.textAlignment = .left
            self.label_sarafiName.textAlignment = .left
            self.label_email.textAlignment = .left
            self.label_mob.textAlignment = .left
            self.label_name.textAlignment = .left
            
            self.label_accoutnDetails.textAlignment = .left
            self.label_personalDetaisl.textAlignment = .left
            
            self.label_myProfile.text = ("My Profile".localizeStringUsingSystemLang)
            self.label_personalDetaisl.text = ("Personal Details".localizeStringUsingSystemLang)
            self.label_accoutnDetails.text = ("Account Details".localizeStringUsingSystemLang)
        }
        else if(language == "دری"){
           superView.semanticContentAttribute = .forceRightToLeft
            self.label_licenseExpiryDate.textAlignment = .right
            self.label_licenseNo.textAlignment = .right
            self.label_offNo.textAlignment = .right
            self.label_sarafiName.textAlignment = .right
            self.label_email.textAlignment = .right
            self.label_mob.textAlignment = .right
            self.label_name.textAlignment = .right
            
            self.label_accoutnDetails.textAlignment = .right
            self.label_personalDetaisl.textAlignment = .right
            
            self.label_myProfile.text = ("My Profile".localizeString1(lang: "fa-AF"))
            self.label_personalDetaisl.text = ("Personal Details".localizeString1(lang: "fa-AF"))
            self.label_accoutnDetails.text = ("Account Details".localizeString1(lang: "fa-AF"))
        }
        else {
            superView.semanticContentAttribute = .forceRightToLeft
            self.label_licenseExpiryDate.textAlignment = .right
            self.label_licenseNo.textAlignment = .right
            self.label_offNo.textAlignment = .right
            self.label_sarafiName.textAlignment = .right
            self.label_email.textAlignment = .right
            self.label_mob.textAlignment = .right
            self.label_name.textAlignment = .right
            
            self.label_accoutnDetails.textAlignment = .right
            self.label_personalDetaisl.textAlignment = .right
            
            self.label_myProfile.text = ("My Profile".localizeString1(lang: "ps-AF"))
            self.label_personalDetaisl.text = ("Personal Details".localizeString1(lang: "ps-AF"))
            self.label_accoutnDetails.text = ("Account Details".localizeString1(lang: "ps-AF"))
        }
    }
    
    @IBAction func acion_logout(_ sender: Any) {
        
        
        UserDefaults.standard.removeObject(forKey: "AppLanguageKey")
        UserDefaults.standard.removeObject(forKey: "Password")
        UserDefaults.standard.removeObject(forKey: "MobileNo")
        UserDefaults.standard.removeObject(forKey: "lid")
        UserDefaults.standard.removeObject(forKey: "auth_token")
        UserDefaults.standard.removeObject(forKey: "exm_id")
        UserDefaults.standard.removeObject(forKey: "fname")
        UserDefaults.standard.removeObject(forKey: "lname")
        
        User.logOutUser { (status) in
            if status == true {
               // self.dismiss(animated: true, completion: nil)
            }
        }
        
        self.navigationController?.popToRootViewController(animated: true)
        
        
    }
    
    
    func getUserInfo(){
     
        let usertype = UserDefaults.standard.value(forKey: "LoginType") as! String
        var agent = ""
        if(usertype == "user"){
            agent  = "0"
            
            self.getUserDetails(agent: agent)
        }
        else {
            agent = "1"
            self.getAgentDetails(agent: agent)
        }
        
        print("agent value = \(agent)")
        
        
        /*
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            let auth_token = UserDefaults.standard.value(forKey: "auth_token") as! String
            print(auth_token)
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            
            
            Alamofire.request("http://qtademo.com/saraf/api/p_details", method: .post , parameters: ["uid" : UserDefaults.standard.value(forKey: "uid")! , "agent" : agent], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        
                        if(json["data"] != nil){
                            
                            self.label_name.text = ((json["data"] as! NSDictionary).value(forKey: "fname") as! String) + " " +  ((json["data"] as! NSDictionary).value(forKey: "lname") as! String)
                            self.label_mob.text = (json["data"] as! NSDictionary).value(forKey: "mobile") as? String
                            self.label_email.text = (json["data"] as! NSDictionary).value(forKey: "email") as? String
                            self.label_sarafiName.text = (json["data"] as! NSDictionary).value(forKey: "rgtd_license_name") as? String
                            self.label_offNo.text = (json["data"] as! NSDictionary).value(forKey: "office_no") as? String
                            self.label_licenseNo.text = (json["data"] as! NSDictionary).value(forKey: "license_no") as? String
                            self.label_licenseExpiryDate.text = (json["data"] as! NSDictionary).value(forKey: "license_exp_date") as? String
                        }
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
        */
        
    }
    
    func getUserDetails(agent : String){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            let auth_token = UserDefaults.standard.value(forKey: "auth_token") as! String
            print(auth_token)
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            
            
            Alamofire.request("http://qtademo.com/saraf/api/p_details", method: .post , parameters: ["uid" : UserDefaults.standard.value(forKey: "uid")! ], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        
                        if(json["data"] != nil){
                            
                            self.label_name.text = ((json["data"] as! NSDictionary).value(forKey: "fname") as! String) + " " +  ((json["data"] as! NSDictionary).value(forKey: "lname") as! String)
                            self.label_mob.text = (json["data"] as! NSDictionary).value(forKey: "mobile") as? String
                            self.label_email.text = (json["data"] as! NSDictionary).value(forKey: "email") as? String
                            self.label_sarafiName.text = (json["data"] as! NSDictionary).value(forKey: "rgtd_license_name") as? String
                            self.label_offNo.text = (json["data"] as! NSDictionary).value(forKey: "office_no") as? String
                            self.label_licenseNo.text = (json["data"] as! NSDictionary).value(forKey: "license_no") as? String
                            self.label_licenseExpiryDate.text = (json["data"] as! NSDictionary).value(forKey: "license_exp_date") as? String
                        }
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func getAgentDetails(agent : String){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            let auth_token = UserDefaults.standard.value(forKey: "auth_token") as! String
            print(auth_token)
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            
            
            Alamofire.request("http://qtademo.com/saraf/api/agent_details", method: .post , parameters: ["aid" : "18" ], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        
                        if(json["data"] != nil){
                            
                            self.label_name.text = ((json["data"] as! NSDictionary).value(forKey: "fname") as! String) + " " +  ((json["data"] as! NSDictionary).value(forKey: "lname") as! String)
                            self.label_mob.text = (json["data"] as! NSDictionary).value(forKey: "mobile") as? String
                            self.label_email.text = (json["data"] as! NSDictionary).value(forKey: "email") as? String
                            self.label_sarafiName.text = (json["data"] as! NSDictionary).value(forKey: "rgtd_license_name") as? String
                            self.label_offNo.text = (json["data"] as! NSDictionary).value(forKey: "office_no") as? String
                            self.label_licenseNo.text = (json["data"] as! NSDictionary).value(forKey: "license_no") as? String
                            self.label_licenseExpiryDate.text = (json["data"] as! NSDictionary).value(forKey: "license_exp_date") as? String
                        }
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

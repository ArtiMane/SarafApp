//
//  tabbarController.swift
//  localization
//
//  Created by Developer on 5/13/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class tabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Helvetica", size: 12)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Helvetica", size: 12)!], for: .selected)
        
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            
            self.tabBar.items?[0].title = "Rates".localizeStringUsingSystemLang
            self.tabBar.items?[1].title = "Posts".localizeStringUsingSystemLang
            self.tabBar.items?[2].title = "My Posts".localizeStringUsingSystemLang
            self.tabBar.items?[3].title = "Profile".localizeStringUsingSystemLang
        }
        else if(language == "دری"){
            
            self.tabBar.items?[0].title = "Rates".localizeString1(lang: "fa-AF")
            self.tabBar.items?[1].title = "Posts".localizeString1(lang: "fa-AF")
            self.tabBar.items?[2].title = "My Posts".localizeString1(lang: "fa-AF")
            self.tabBar.items?[3].title = "Profile".localizeString1(lang: "fa-AF")
        }
        else {
            
            self.tabBar.items?[0].title = "Rates".localizeString1(lang: "ps-AF")
            self.tabBar.items?[1].title = "Posts".localizeString1(lang: "ps-AF")
            self.tabBar.items?[2].title = "My Posts".localizeString1(lang: "ps-AF")
            self.tabBar.items?[3].title = "Profile".localizeString1(lang: "ps-AF")
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            self.view.semanticContentAttribute = .forceLeftToRight
        }
        else {
            self.view.semanticContentAttribute = .forceRightToLeft
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

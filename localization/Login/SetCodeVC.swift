//
//  SetCodeVC.swift
//  localization
//
//  Created by Developer on 5/1/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class SetCodeVC: UIViewController {

    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var viewSuper: UIView!
    @IBOutlet weak var viewCode: UIView!
    @IBOutlet weak var buttonSent: UIButton!
    @IBOutlet weak var textCode: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        textCode.attributedPlaceholder = NSAttributedString(string: "Enter code",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        viewCode.layer.cornerRadius = 3.0
        buttonSent.layer.cornerRadius = 3.0
    }

    @IBAction func action_setCode(_ sender: Any) {
        
        let resetPassword : ResetPasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
        self.navigationController?.pushViewController(resetPassword, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(UserDefaults.standard.value(forKey: "selectedLanguage") as! String != "English"){
             self.viewSuper.semanticContentAttribute = .forceRightToLeft
            self.viewCode.semanticContentAttribute = .forceRightToLeft
            self.textCode.textAlignment  = .right
            self.labelText.textAlignment = .right
            self.buttonBack.setImage(UIImage(named:"rightArrow.png"), for: .normal)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_back(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

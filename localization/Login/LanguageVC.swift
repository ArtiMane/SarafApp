//
//  LanguageVC.swift
//  localization
//
//  Created by Developer on 4/30/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire


class LanguageVC: UIViewController ,UIPickerViewDelegate , UIPickerViewDataSource{

    @IBOutlet weak var buttonSignIn: UIButton!
    @IBOutlet weak var buttonSignup: UIButton!
    @IBOutlet weak var label_welcome: UILabel!
    @IBOutlet weak var label_signIn: UILabel!
    @IBOutlet weak var label_newUser: UILabel!
    @IBOutlet weak var viewLanguage: UIView!
    @IBOutlet weak var label_selectLanguage: UILabel!
    @IBOutlet weak var textLanguage: UITextField!
    var pickerView = UIPickerView()
    var arrayLanguage = [String]()
    var arrayLanguageId = [String]()
    var selectedLanguage = ""
    var selectedLanguageId = ""
    
    @IBOutlet weak var button_signUp: UIButton!
    
    @IBOutlet weak var button_signIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewLanguage.layer.cornerRadius = 3.0
        buttonSignIn.layer.cornerRadius = 3.0
        buttonSignup.layer.cornerRadius = 3.0
       // arrayLanguage = ["English","Farsi","Pashtu"]
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        textLanguage.inputView = pickerView
        self.textLanguage.text = "English"
        self.textLanguage.textAlignment = .left
        self.textLanguage.attributedPlaceholder = NSAttributedString(string: "Select language",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        
        UserDefaults.standard.setValue("English", forKey: "AppLanguageKey")
        
        self.getLanguage()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        UserDefaults.standard.setValue("English", forKey: "AppLanguageKey")
        UserDefaults.standard.set("1", forKey: "lid")
        if(UserDefaults.standard.value(forKey: "auth_token") != nil){
         //  self.callLoginAPI()
            
            let regisetr : tabbarController = self.storyboard?.instantiateViewController(withIdentifier: "tabbarController") as! tabbarController
            self.navigationController?.pushViewController(regisetr, animated: true)
        }
    }
    
    func callLoginAPI(){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            loadingNotification.label.text = "Loading"
            
            
            Alamofire.request("http://qtademo.com/saraf/api/user_login", method: .post, parameters: ["m":UserDefaults.standard.value(forKey: "MobileNo")! , "p" : UserDefaults.standard.value(forKey: "Password")!], encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        let data = json["data"] as! NSDictionary
                        let authrtoken = json["auth_token"] as! String
                        let exm_id = data["exm_id"] as! String
                        print("auth token = \(authrtoken)")
                        UserDefaults.standard.set(authrtoken, forKey: "auth_token")
                        UserDefaults.standard.set(exm_id, forKey: "exm_id")
                        UserDefaults.standard.set(data["uid"], forKey: "uid")
                        
                        let lid = data["lid"] as! String
                        var selectedLanguageId = ""
                        if(lid == "1"){
                            selectedLanguageId = "English"
                        }
                        if(lid == "2"){
                            selectedLanguageId = "دری"
                        }
                        if(lid == "3"){
                            selectedLanguageId = "pasto"
                        }
                        UserDefaults.standard.set(selectedLanguageId, forKey: "AppLanguageKey")
                        
                        let regisetr : tabbarController = self.storyboard?.instantiateViewController(withIdentifier: "tabbarController") as! tabbarController
                        self.navigationController?.pushViewController(regisetr, animated: true)
                    }
                    
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    // get language
    
    func getLanguage(){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            loadingNotification.label.text = "Loading..."
            
            
            Alamofire.request("http://qtademo.com/saraf/api/lang", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let arrayLanguage : NSArray = json["lang_list"] as! NSArray
                    
                    for var i in 0..<arrayLanguage.count{
                        
                        let language = (arrayLanguage[i] as! NSDictionary).value(forKey: "lang_name")
                        self.arrayLanguage.append(language as! String)
                        
                        let languageId = (arrayLanguage[i] as! NSDictionary).value(forKey: "lid")
                        self.arrayLanguageId.append(languageId as! String)
                    }
                    self.pickerView.reloadAllComponents()
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func actionProceed(_ sender: Any) {
        
        if(selectedLanguage == ""){
            
            let alertController : UIAlertController = UIAlertController(title: "Message", message: "Please select language", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alertController.dismiss(animated: true, completion: nil)
                
            })
            alertController.addAction(actionSheet)
            self.present(alertController, animated: true, completion: nil)
            
        }
        else {
        UserDefaults.standard.set(selectedLanguage, forKey: "selectedLanguage")
        let login : LoginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(login, animated: true)
        }
    }
    
    //MARK: - Pickerview method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayLanguage.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayLanguage[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textLanguage.text = arrayLanguage[row]
        selectedLanguage = arrayLanguage[row]
        selectedLanguageId = arrayLanguageId[row]
        print(selectedLanguage)
        UserDefaults.standard.set(selectedLanguage, forKey: "AppLanguageKey")
        UserDefaults.standard.set(selectedLanguageId, forKey: "lid")
        self.changeAttributes()
        //self.view.endEditing(true)
    }
    
    @IBAction func action_signUp(_ sender: Any) {
     
        let regisetr : Register1 = self.storyboard?.instantiateViewController(withIdentifier: "Register1") as! Register1
        self.navigationController?.pushViewController(regisetr, animated: true)
    }
    
    @IBAction func action_signIn(_ sender: Any) {
        let regisetr : UserLogin = self.storyboard?.instantiateViewController(withIdentifier: "UserLogin") as! UserLogin
        self.navigationController?.pushViewController(regisetr, animated: true)
    }
    
    func changeAttributes(){
        
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        
        if(language == "English"){
            textLanguage.textAlignment = .left
            self.label_welcome.text = ("Welcome to Saraf".localizeStringUsingSystemLang)
            self.label_selectLanguage.text = ("Select Language to Proceed".localizeStringUsingSystemLang)
            self.label_newUser.text = ("New User? Sign Up".localizeStringUsingSystemLang)
            self.label_signIn.text = ("Joined Already? Sign In".localizeStringUsingSystemLang)
            self.button_signUp.setTitle(("Sign Up".localizeStringUsingSystemLang), for: .normal)
            self.button_signIn.setTitle(("Sign In".localizeStringUsingSystemLang), for: .normal)
            self.viewLanguage.semanticContentAttribute = .forceLeftToRight
            
        }
        else if(language == "دری"){
            textLanguage.textAlignment = .right
            self.label_welcome.text = ("Welcome to Saraf".localizeString1(lang: "fa-AF"))
            self.label_selectLanguage.text = ("Select Language to Proceed".localizeString1(lang: "fa-AF"))
            self.label_newUser.text = ("New User? Sign Up".localizeString1(lang: "fa-AF"))
            self.label_signIn.text = ("Joined Already? Sign In".localizeString1(lang: "fa-AF"))
            self.button_signUp.setTitle("Sign Up".localizeString1(lang: "fa-AF"), for: .normal)
            self.button_signIn.setTitle("Sign In".localizeString1(lang: "fa-AF"), for: .normal)
            self.viewLanguage.semanticContentAttribute = .forceRightToLeft
        }
        else {
            textLanguage.textAlignment = .right
            self.label_welcome.text = ("Welcome to Saraf".localizeString1(lang: "ps-AF"))
            self.label_selectLanguage.text = ("Select Language to Proceed".localizeString1(lang: "ps-AF"))
            self.label_newUser.text = ("New User? Sign Up".localizeString1(lang: "ps-AF"))
            self.label_signIn.text = ("Joined Already? Sign In".localizeString1(lang: "ps-AF"))
            self.button_signUp.setTitle("Sign Up".localizeString1(lang: "ps-AF"), for: .normal)
            self.button_signIn.setTitle("Sign In".localizeString1(lang: "ps-AF"), for: .normal)
            self.viewLanguage.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  MyPostsCell.swift
//  localization
//
//  Created by Developer on 5/15/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class MyPostsCell: UITableViewCell {

    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var label_sun: UILabel!
    @IBOutlet weak var label_from: UILabel!
    @IBOutlet weak var label_to1: UILabel!
    @IBOutlet weak var label_to: UILabel!
    @IBOutlet weak var label_amount: UILabel!
    @IBOutlet weak var label_sell: UILabel!
    @IBOutlet weak var label_name1: UILabel!
    @IBOutlet weak var label_name: UILabel!
    @IBOutlet weak var superView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ResetPasswordVC.swift
//  localization
//
//  Created by Developer on 5/1/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {

    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var viewSuper: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewReEnterPassword: UIView!
    @IBOutlet weak var buttonSet: UIButton!
    @IBOutlet weak var textResetPassword: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        textPassword.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textResetPassword.attributedPlaceholder = NSAttributedString(string: "Re-enter password",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        viewPassword.layer.cornerRadius = 3.0
        viewReEnterPassword.layer.cornerRadius = 3.0
        buttonSet.layer.cornerRadius = 3.0
    }

    @IBAction func action_setPassword(_ sender: Any) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(UserDefaults.standard.value(forKey: "selectedLanguage") as! String != "English"){
            
            self.viewPassword.semanticContentAttribute = .forceRightToLeft
            self.viewReEnterPassword.semanticContentAttribute = .forceRightToLeft
            self.textResetPassword.textAlignment  = .right
             self.viewSuper.semanticContentAttribute = .forceRightToLeft
            self.textPassword.textAlignment  = .right
            self.labelText.textAlignment = .right
            self.buttonBack.setImage(UIImage(named:"rightArrow.png"), for: .normal)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_back(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

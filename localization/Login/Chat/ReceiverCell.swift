//
//  ReceiverCell.swift
//  localization
//
//  Created by Developer on 5/19/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class ReceiverCell: UITableViewCell {

 
    @IBOutlet weak var message: PaddingLabel!
    
    @IBOutlet weak var messageBackground: UIImageView!
    
    func clearCellData()  {
        self.message.text = nil
        self.message.isHidden = false
        self.messageBackground.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.message.drawText(in: CGRect(x: 5.0, y: 5.0, width: 5.0, height: 5.0))
        //        self.message.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5)
        self.messageBackground.layer.cornerRadius = 15
        self.messageBackground.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

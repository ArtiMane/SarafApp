//
//  Chat.swift
//  localization
//
//  Created by Developer on 5/19/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Firebase
import Photos
import CoreLocation
import Alamofire

class Chat: UIViewController , UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,  UINavigationControllerDelegate, UIImagePickerControllerDelegate, CLLocationManagerDelegate, UIDocumentMenuDelegate, UIDocumentPickerDelegate {

    
    @IBOutlet weak var label_name: UILabel!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet var inputBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    var post_id = ""
    var self_user_id = ""
    var post_user_id = ""
    var oherMobileNo = ""
    var post_name = ""
    var documentPicker = UIDocumentPickerViewController(documentTypes:  ["public.data"], in:  UIDocumentPickerMode.import)
    
    override var inputAccessoryView: UIView? {
        get {
            self.inputBar.frame.size.height = self.barHeight
            self.inputBar.clipsToBounds = true
            return self.inputBar
        }
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    let locationManager = CLLocationManager()
    var items = [Message]()
    var items1 = [Message]()
    let imagePicker = UIImagePickerController()
    let barHeight: CGFloat = 50
    var currentUser: User?
    var canSendLocation = true
    
    //MARK: Methods
    func customization() {
        
//        self.inputBar.frame.size.height = self.barHeight
//        self.inputBar.clipsToBounds = true
        self.imagePicker.delegate = self
       // self.tableView.estimatedRowHeight = self.barHeight
     //   self.tableView.rowHeight = UITableViewAutomaticDimension
      //  self.tableView.contentInset.bottom = self.barHeight
     //   self.tableView.scrollIndicatorInsets.bottom = self.barHeight
        self.navigationController?.navigationBar.backgroundColor = UIColor.black
        self.navigationItem.title = self.currentUser?.name
        self.navigationItem.setHidesBackButton(true, animated: false)
        let icon = UIImage.init(named: "myBack")?.withRenderingMode(.alwaysOriginal)
        let backButton = UIBarButtonItem.init(image: icon!, style: .plain, target: self, action: #selector(self.dismissSelf))
        self.navigationItem.leftBarButtonItem = backButton
        self.locationManager.delegate = self
    }
    
    
    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //Downloads messages
    func fetchData() {
        let mobileNo = UserDefaults.standard.value(forKey: "MobileNo") as! String
        
       
        Message.downloadAllMessages(forUserID: mobileNo, completion: {[weak weakSelf = self] (message) in
            
            print("called")
            if(UserDefaults.standard.value(forKey: "dataValueAdded") != nil){

                let value = UserDefaults.standard.value(forKey: "dataValueAdded") as! String
                if(value == "1"){

                    weakSelf?.items.removeAll()
                    UserDefaults.standard.set("0", forKey: "dataValueAdded")
                }
                else {
                    weakSelf?.items.append(message)
                    weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
                    DispatchQueue.main.async {
                        // UserDefaults.standard.set("1", forKey: "dataValueAdded")
                        if let state = weakSelf?.items.isEmpty, state == false {
                            print("called tableview")
                            weakSelf?.tableView.reloadData()
                            // UserDefaults.standard.set("1", forKey: "dataValueAdded")
                            weakSelf?.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                        }
                    }
                }
            }
            else {
                weakSelf?.items.append(message)
                weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
                DispatchQueue.main.async {
                    // UserDefaults.standard.set("1", forKey: "dataValueAdded")
                    if let state = weakSelf?.items.isEmpty, state == false {
                        print("called tableview")
                        weakSelf?.tableView.reloadData()
                        // UserDefaults.standard.set("1", forKey: "dataValueAdded")
                        weakSelf?.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                    }
                }
            }
            
        })
       // Message.markMessagesRead(forUserID: mobileNo)
    }
    
    //Hides current viewcontroller
    @objc func dismissSelf() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func composeMessage(type: MessageType, content: Any)  {
        let message = Message.init(type: type, content: content, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, fromId: UserDefaults.standard.value(forKey: "MobileNo") as! String)
        Message.send(message: message, toID: oherMobileNo, completion: {(_) in
        })
        
        switch type {
        case .text:
            
            if CheckConnection.isConnectedToNetwork() {
                print("Connected")
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = .indeterminate
                //    loadingNotification.label.text = "Registering"
                let auth_token = UserDefaults.standard.value(forKey: "auth_token") as! String
                print(auth_token)
                let headers : HTTPHeaders = [
                    "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String ,
                    "ENCTYPE" : "multipart/form-data"
                ]
                
                print(post_id)
                print(UserDefaults.standard.value(forKey: "uid"))
                print(post_user_id)
                
                Alamofire.request("http://qtademo.com/saraf/api/insert_chat_message", method: .post , parameters: ["exp_id" : post_id , "s_uid" : UserDefaults.standard.value(forKey: "uid")! , "r_uid" : post_user_id ,"mf" : "1" , "msg" : content as! String], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    switch response.result {
                        
                        
                    case .success:
                        
                        
                        print(response)
                        
                        
                        
                        
                        
                        break
                    case .failure(let error):
                        
                        print(error)
                        let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(actionSheet)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                })
                
            }
            else{
                let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
                let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                })
                alert.addAction(actionSheet)
                self.present(alert, animated: true, completion: nil)
            }
            
            print("hi")
            
        case .photo :
            print("hi")
            
            if let image = content as? UIImage {
                //  self.imagePickedBlock?(image)
                
                
                
                // let image = UIImage.init(named: "myImage")
                let imgData = UIImageJPEGRepresentation(image, 0.2)!
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = .indeterminate
                
                
                
                let params: Parameters = ["exp_id" : post_id , "s_uid" : UserDefaults.standard.value(forKey: "uid")! , "r_uid" : post_user_id ,"mf" : "2"]
                Alamofire.upload(multipartFormData:
                    {
                        (multipartFormData) in
                        multipartFormData.append(UIImageJPEGRepresentation(image, 0.1)!, withName: "imgfile", fileName: "imgfile.jpeg", mimeType: "image/jpeg")
                        for (key, value) in params
                        {
                            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                        }
                }, to:"http://qtademo.com/saraf/api/insert_chat_message",headers: ["auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String , "ENCTYPE": "multipart/form-data"])
                { (result) in
                    
                    
                    
                    switch result {
                    case .success(let upload,_,_ ):
                        upload.uploadProgress(closure: { (progress) in
                            //Print progress
                            print(progress)
                        })
                        upload.responseJSON
                            { response in
                                //print response.result
                                MBProgressHUD.hide(for: self.view, animated: true)
                                if response.result.value != nil
                                {
                                    print(response)
                                    switch response.result {
                                        
                                    case .success :
                                        
                                        
                                        break
                                        
                                    case .failure(let error):
                                        let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                                            alert.dismiss(animated: true, completion: nil)
                                        })
                                        alert.addAction(actionSheet)
                                        self.present(alert, animated: true, completion: nil)
                                        break
                                        
                                        
                                    }
                                    
                                    
                                    
                                }
                        }
                    case .failure(let encodingError):
                        
                        let alert : UIAlertController = UIAlertController(title: "Error", message: encodingError.localizedDescription, preferredStyle: .actionSheet)
                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(actionSheet)
                        self.present(alert, animated: true, completion: nil)
                        break
                    
                    
             
                    }
                }
            }
 
            
        default:
            break
        }
        
        
        
    }
    
    func checkLocationPermission() -> Bool {
        var state = false
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            state = true
        case .authorizedAlways:
            state = true
        default: break
        }
        return state
    }
    
    func animateExtraButtons(toHide: Bool)  {
        switch toHide {
        case true:
            self.bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        default:
            self.bottomConstraint.constant = -50
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        UserDefaults.standard.setValue("25", forKey: "post_id")
//        UserDefaults.standard.setValue("32", forKey: "self_user_id")
//        UserDefaults.standard.setValue("31", forKey: "post_user_id")
//        UserDefaults.standard.setValue("7798931066", forKey: "otherMobileNo")
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.tableView, animated: true)
        loadingNotification.mode = .indeterminate
        loadingNotification.label.text = "Loading"
        
        self.label_name.text = self.post_name
        UserDefaults.standard.setValue(self.post_id, forKey: "post_id")
        UserDefaults.standard.setValue(self.self_user_id, forKey: "self_user_id")
        UserDefaults.standard.setValue(self.post_user_id, forKey: "post_user_id")
        UserDefaults.standard.setValue(oherMobileNo, forKey: "otherMobileNo")
        
        self.customization()
        self.fetchData()
    }

    
    @IBAction func showMessage(_ sender: Any) {
        
        self.animateExtraButtons(toHide: true)
    }
    
    
    @IBAction func selectGallery(_ sender: Any) {
        
        self.animateExtraButtons(toHide: true)
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .savedPhotosAlbum;
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func selectCamera(_ sender: Any) {
        
        self.animateExtraButtons(toHide: true)
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func selectLocation(_ sender: Any) {
        
//        self.canSendLocation = true
//        self.animateExtraButtons(toHide: true)
//        if self.checkLocationPermission() {
//            self.locationManager.startUpdatingLocation()
//        } else {
//            self.locationManager.requestWhenInUseAuthorization()
//        }
        
        let importMenu = UIDocumentMenuViewController(documentTypes: ["public.data"], in: UIDocumentPickerMode.import)
        
        importMenu.delegate = self
        
        self.present(importMenu, animated: true, completion: nil)
    }
    
    
    @IBAction func sendMessage(_ sender: Any) {
        
        if let text = self.inputTextField.text {
            if text.characters.count > 0 {
                self.composeMessage(type: .text, content: self.inputTextField.text!)
                self.inputTextField.text = ""
            }
        }
        
    }
    
    @IBAction func showOptions(_ sender: Any) {
        
         self.animateExtraButtons(toHide: false)
    }
    
    //MARK: NotificationCenter handlers
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.tableView.contentInset.bottom = height
            self.tableView.scrollIndicatorInsets.bottom = height
            if self.items.count > 0 {
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    //MARK:  Document INTERACTION
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        
        
        documentPicker.delegate = self
        
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        print(url)
        //  self.uploadFile(filePath: url)
        self.uploadFilepdf(filePath: url)
        
    }
    
    func uploadFilepdf(filePath : URL){
        
        
        print("filepath = \(filePath)")
        let path = filePath.absoluteString
        if(path.contains(".aiff")){
            
            self.composeMessage(type: .audio , content: filePath)
        }
        else
        {
            
            let alert : UIAlertController = UIAlertController(title: "Error", message: "The file type you are attempting to upload is not allowed.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
            /*
            
            Alamofire.upload(
                multipartFormData: {
                    multipartFormData in
                    
                    let params: Parameters = ["exp_id" : self.post_id , "s_uid" : UserDefaults.standard.value(forKey: "uid")! , "r_uid" : self.post_user_id ,"mf" : "3"]
                    
                    let pdfData = try! Data(contentsOf:filePath)
                    var data : Data = pdfData
                    
                    multipartFormData.append(data as Data, withName:"audiofile",fileName: "audiofile", mimeType:"audio/m4a")
                    for (key, value) in params {
                        multipartFormData.append(((value as? String)?.data(using: .utf8))!, withName: key)
                    }
                    
                    print("Multi part Content -Type")
                    print(multipartFormData.contentType)
                    print("Multi part FIN ")
                    print("Multi part Content-Length")
                    print(multipartFormData.contentLength)
                    print("Multi part Content-Boundary")
                    print(multipartFormData.boundary)
                    
            },
                usingThreshold : SessionManager.multipartFormDataEncodingMemoryThreshold,
                to: "http://qtademo.com/saraf/api/insert_chat_message",
                method: .post,
                headers: ["auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String ,"ENCTYPE": "multipart/form-data"],
                encodingCompletion: { encodingResult in
                    
                    switch encodingResult {
                        
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            print(" responses ")
                            print(response)
                            print("end responses")
                            
                            
                }
                        break
                    case .failure(let encodingError):
                        print(encodingError)
                        //  onCompletion(false, "Something bad happen...", 200)
                        let alert : UIAlertController = UIAlertController(title: "Error", message: encodingError.localizedDescription, preferredStyle: .actionSheet)
                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(actionSheet)
                        self.present(alert, animated: true, completion: nil)
                        break
                    }
            })
        }
        else
        {
            
            let alert : UIAlertController = UIAlertController(title: "Error", message: "The file type you are attempting to upload is not allowed.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
 */
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    //MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        UserDefaults.standard.set("1", forKey: "dataValueAdded")
        MBProgressHUD.hide(for: self.tableView, animated: true)
        print(self.items.count)
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging {
            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, animations: {
                cell.transform = CGAffineTransform.identity
            })
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       MBProgressHUD.hide(for: self.tableView, animated: true)
        let fromId = self.items[indexPath.row].fromId as! String
        let mobileNo = UserDefaults.standard.value(forKey: "MobileNo") as! String
        print("fromId\(fromId)")
        print("mobileNo\(mobileNo)")
         var type = ""
        if fromId == mobileNo {
            type = "receiver"
        }
        else {
            type = "sender"
        }
       
        switch self.items[indexPath.row].owner {
      
        case .receiver :
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
            cell.clearCellData()
            switch self.items[indexPath.row].type {
            case .text:
                cell.message.text = self.items[indexPath.row].content as! String
            case .photo:
                if let image = self.items[indexPath.row].image {
                    cell.messageBackground.frame = CGRect(x: cell.messageBackground.frame.origin.x, y: cell.messageBackground.frame.origin.y, width: cell.messageBackground.frame.size.width, height: 300)
                    cell.messageBackground.image = image
                    cell.message.isHidden = true
                } else {
                    cell.messageBackground.image = UIImage(named: "loading.png")
                    cell.messageBackground.frame = CGRect(x: cell.messageBackground.frame.origin.x, y: cell.messageBackground.frame.origin.y, width: cell.messageBackground.frame.size.width, height: 300)
                    let path = self.items[indexPath.row].content as! String
                    if(path == ""){
                        
                    }
                    else {
                    self.items[indexPath.row].downloadImage(indexpathRow: indexPath.row, completion: { (state, index) in
                        if state == true {
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                    })
                    }
                }
            case .location:
                cell.messageBackground.image = UIImage.init(named: "location")
                cell.message.isHidden = true
            case .audio:
                print("hello")
            }
            return cell
        
        case .sender :
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
            cell.clearCellData()
            cell.profilePic.image = self.currentUser?.profilePic
            switch self.items[indexPath.row].type {
            case .text:
                cell.message.text = self.items[indexPath.row].content as! String
            case .photo:
                if let image = self.items[indexPath.row].image {
                    cell.messageBackground.image = image
                    cell.message.isHidden = true
                } else {
                    cell.messageBackground.image = UIImage.init(named: "loading")
                    self.items[indexPath.row].downloadImage(indexpathRow: indexPath.row, completion: { (state, index) in
                        if state == true {
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                    })
                }
            case .location:
                cell.messageBackground.image = UIImage.init(named: "location")
                cell.message.isHidden = true
            case .audio:
                print("hello")
            }
            return cell

            }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.inputTextField.resignFirstResponder()
        switch self.items[indexPath.row].type {
        case .photo:
            if let photo = self.items[indexPath.row].image {
                let info = ["viewType" : ShowExtraView.preview, "pic": photo] as [String : Any]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
             //   self.inputAccessoryView?.isHidden = true
            }
        case .location:
            let coordinates = (self.items[indexPath.row].content as! String).components(separatedBy: ":")
            let location = CLLocationCoordinate2D.init(latitude: CLLocationDegrees(coordinates[0])!, longitude: CLLocationDegrees(coordinates[1])!)
            let info = ["viewType" : ShowExtraView.map, "location": location] as [String : Any]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
          //  self.inputAccessoryView?.isHidden = true
        default: break
        }
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.composeMessage(type: .photo, content: pickedImage)
        } else {
            let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            self.composeMessage(type: .photo, content: pickedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        if let lastLocation = locations.last {
            if self.canSendLocation {
                let coordinate = String(lastLocation.coordinate.latitude) + ":" + String(lastLocation.coordinate.longitude)
                let message = Message.init(type: .location, content: coordinate, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, fromId: "9420429240")
                Message.send(message: message, toID: self.currentUser!.id, completion: {(_) in
                })
                self.canSendLocation = false
            }
        }
    }
    
    //MARK: ViewController lifecycle
    override func viewDidAppear(_ animated: Bool) {
       
      //  inputAccessoryView?.isHidden = false
        super.viewDidAppear(animated)
//        self.inputBar.frame.size.height = self.barHeight
//        self.inputBar.clipsToBounds = true
        self.inputBar.backgroundColor = UIColor.clear
        self.view.layoutIfNeeded()
        NotificationCenter.default.addObserver(self, selector: #selector(Chat.showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        let mobileNumber = UserDefaults.standard.value(forKey: "MobileNo")
        Message.markMessagesRead(forUserID: mobileNumber as! String )
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

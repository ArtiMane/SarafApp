//
//  PostDetails.swift
//  localization
//
//  Created by Developer on 5/16/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire
import CoreTelephony
import CallKit

class PostDetails: UIViewController , CXCallObserverDelegate {

    @IBOutlet weak var image_profile: UIImageView!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var label_postDetails: UILabel!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var superView: UIView!
    
    @IBOutlet weak var label_name: UILabel!
    
    @IBOutlet weak var button_call: UIButton!
    @IBOutlet weak var label_description1: UILabel!
    @IBOutlet weak var label_description: UILabel!
    @IBOutlet weak var label_toCurrency1: UILabel!
    @IBOutlet weak var label_toCurrency: UILabel!
    @IBOutlet weak var label_fromCurrency1: UILabel!
    @IBOutlet weak var label_fromCurrency: UILabel!
    @IBOutlet weak var label_amount1: UILabel!
    @IBOutlet weak var label_amount: UILabel!
    @IBOutlet weak var label_market1: UILabel!
    @IBOutlet weak var label_market: UILabel!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var label_name1: UILabel!
    
    @IBOutlet weak var button_chat: UIButton!
    var exp_id = ""
    var mobileNumber = ""
    var ea_uid = ""
    var callObserver = CXCallObserver()
    var post_uid = ""
    var self_id = ""
    var otherMobileNo = ""
    var post_fname = ""
    var post_lname = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.button_chat.layer.cornerRadius = 3.0
        self.button_call.layer.cornerRadius = 3.0
        
        callObserver.setDelegate(self, queue: nil)
        
        self_id = UserDefaults.standard.value(forKey: "uid") as! String
        self.fetchPostDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            self.upperView.semanticContentAttribute = .forceLeftToRight
            self.superView.semanticContentAttribute = .forceLeftToRight
            self.myScrollView.semanticContentAttribute = .forceLeftToRight
            
            self.buttonBack.setImage(UIImage(named : "leftArrow.png"), for: .normal)
           // self.butt.setTitle("Register".localizeString1(lang: "fa-AF"), for: .normal)
          // self.button_register.setTitle("Register".localizeString1(lang: "fa-AF"), for: .normal)
            self.label_postDetails.text = ("Post Details".localizeStringUsingSystemLang)
            self.label_description.text = ("Description".localizeStringUsingSystemLang)
            self.label_toCurrency.text = ("To Currency".localizeStringUsingSystemLang)
            self.label_fromCurrency.text = ("From Currency".localizeStringUsingSystemLang)
            self.label_market.text = ("Market".localizeStringUsingSystemLang)
            self.label_amount.text = ("Amount".localizeStringUsingSystemLang)
            self.label_name.text = ("Name".localizeStringUsingSystemLang)
            
            self.button_call.setTitle("Call".localizeStringUsingSystemLang, for: .normal)
            self.button_chat.setTitle("Chat".localizeStringUsingSystemLang, for: .normal)
            
            self.label_name.textAlignment = .left
            self.label_name1.textAlignment = .left
            self.label_description.textAlignment = .left
            self.label_description1.textAlignment = .left
            self.label_toCurrency.textAlignment = .left
            self.label_toCurrency1.textAlignment = .left
            self.label_amount1.textAlignment = .left
            self.label_amount.textAlignment = .left
            self.label_fromCurrency.textAlignment = .left
            self.label_fromCurrency1.textAlignment = .left
            self.label_market1.textAlignment = .left
            self.label_market.textAlignment = .left
            
        }
        else if(language == "دری"){
            self.upperView.semanticContentAttribute = .forceRightToLeft
            self.superView.semanticContentAttribute = .forceRightToLeft
            self.myScrollView.semanticContentAttribute = .forceRightToLeft
            
            self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
            
            self.label_postDetails.text = ("Post Details".localizeString1(lang: "fa-AF"))
            self.label_description.text = ("Description".localizeString1(lang: "fa-AF"))
            self.label_toCurrency.text = ("To Currency".localizeString1(lang: "fa-AF"))
            self.label_fromCurrency.text = ("From Currency".localizeString1(lang: "fa-AF"))
            self.label_market.text = ("Market".localizeString1(lang: "fa-AF"))
            self.label_amount.text = ("Amount".localizeString1(lang: "fa-AF"))
            self.label_name.text = ("Name".localizeString1(lang: "fa-AF"))
            
            self.button_call.setTitle("Call".localizeString1(lang: "fa-AF"), for: .normal)
            self.button_chat.setTitle("Chat".localizeString1(lang: "fa-AF"), for: .normal)
            
            self.label_name.textAlignment = .right
            self.label_name1.textAlignment = .right
            self.label_description.textAlignment = .right
            self.label_description1.textAlignment = .right
            self.label_toCurrency.textAlignment = .right
            self.label_toCurrency1.textAlignment = .right
            self.label_amount1.textAlignment = .right
            self.label_amount.textAlignment = .right
            self.label_fromCurrency.textAlignment = .right
            self.label_fromCurrency1.textAlignment = .right
            self.label_market1.textAlignment = .right
            self.label_market.textAlignment = .right
        }
        else {
            self.upperView.semanticContentAttribute = .forceRightToLeft
            self.superView.semanticContentAttribute = .forceRightToLeft
            self.myScrollView.semanticContentAttribute = .forceRightToLeft
            
            self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
            
            self.label_postDetails.text = ("Post Details".localizeString1(lang: "ps-AF"))
            self.label_description.text = ("Description".localizeString1(lang: "ps-AF"))
            self.label_toCurrency.text = ("To Currency".localizeString1(lang: "ps-AF"))
            self.label_fromCurrency.text = ("From Currency".localizeString1(lang: "ps-AF"))
            self.label_market.text = ("Market".localizeString1(lang: "ps-AF"))
            self.label_amount.text = ("Amount".localizeString1(lang: "ps-AF"))
            self.label_name.text = ("Name".localizeString1(lang: "ps-AF"))
            
            self.button_call.setTitle("Call".localizeString1(lang: "ps-AF"), for: .normal)
            self.button_chat.setTitle("Chat".localizeString1(lang: "ps-AF"), for: .normal)
            
            self.label_name.textAlignment = .right
            self.label_name1.textAlignment = .right
            self.label_description.textAlignment = .right
            self.label_description1.textAlignment = .right
            self.label_toCurrency.textAlignment = .right
            self.label_toCurrency1.textAlignment = .right
            self.label_amount1.textAlignment = .right
            self.label_amount.textAlignment = .right
            self.label_fromCurrency.textAlignment = .right
            self.label_fromCurrency1.textAlignment = .right
            self.label_market1.textAlignment = .right
            self.label_market.textAlignment = .right
        }
    }
    
    
    @IBAction func action_call(_ sender: Any) {
        
        guard let number = URL(string: "tel://" + mobileNumber) else { return }
        UIApplication.shared.open(number)
    }
    
    func fetchPostDetails(){
        
       
            
            if CheckConnection.isConnectedToNetwork() {
                print("Connected")
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = .indeterminate
                //    loadingNotification.label.text = "Registering"
                let auth_token = UserDefaults.standard.value(forKey: "auth_token") as! String
                print(auth_token)
                let headers : HTTPHeaders = [
                    "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
                ]
                
                Alamofire.request("http://qtademo.com/saraf/api/post_details", method: .post , parameters: ["exp_id" : exp_id], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    switch response.result {
                        
                        
                    case .success:
                        
                        
                        print(response)
                        
                        let json : [String : Any] = response.result.value as! [String : Any]
                        let code :Int = json["code"] as! Int
                        
                        if(code == 200){
                            
                          //  self.navigationController?.popViewController(animated: true)
                            
                            if( json["data"] as? NSDictionary != nil){
                                
                                let dic : NSDictionary = (json["data"] as? NSDictionary)!
                                self.label_name1.text = (dic["fname"] as! String) +  " " + (dic["lname"] as! String)
                                self.label_market1.text = dic["exm_name"] as? String
                                self.label_amount1.text = dic["amount"] as? String
                                self.label_fromCurrency1.text = dic["buy_cur"] as? String
                                self.label_toCurrency1.text = dic["sell_cur"] as? String
                                self.label_description1.text = dic["comment"] as? String
                                self.mobileNumber = dic["mobile"] as! String
                                self.post_fname = (dic["fname"] as! String) +  " " + (dic["lname"] as! String)
                                
                                self.post_uid = dic["uid"] as! String
                                self.changeUI()
                                
                            }
                        }
                        else {
                            
                            let alert : UIAlertController = UIAlertController(title: "Error", message: "Error while fetching post details.", preferredStyle: .actionSheet)
                            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                                alert.dismiss(animated: true, completion: nil)
                            })
                            alert.addAction(actionSheet)
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        
                        break
                    case .failure(let error):
                        
                        print(error)
                        let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(actionSheet)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                })
                
            }
            else{
                let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
                let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                })
                alert.addAction(actionSheet)
                self.present(alert, animated: true, completion: nil)
            }
        
        
        
    }
    
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        
        if call.isOutgoing {
            
            print("outgoing call")
            
        }
    }
    
    func callCallingMethod(){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            let auth_token = UserDefaults.standard.value(forKey: "auth_token") as! String
            print(auth_token)
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            Alamofire.request("http://qtademo.com/saraf/api/insert_post_call", method: .post , parameters: ["exp_id" : exp_id , "ea_uid" : ea_uid , "c_uid" : UserDefaults.standard.value(forKey: "uid")!], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        
                        //  self.navigationController?.popViewController(animated: true)
                        
                       
                    }
                    else {
                        
                        let alert : UIAlertController = UIAlertController(title: "Error", message: "Erro.", preferredStyle: .actionSheet)
                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(actionSheet)
                      //  self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func changeUI(){
        
        // market
        let marketName : String = self.label_market1.text!
        let queHeight : CGFloat = marketName.height(withConstrainedWidth: self.label_market1.frame.size.width, font: UIFont.systemFont(ofSize: 17))
        self.label_market1.frame = CGRect(x: self.label_market1.frame.origin.x, y: self.label_market1.frame.origin.y, width: self.label_market1.frame.size.width, height: queHeight)
        
        // amount
        self.label_amount.frame = CGRect(x: self.label_amount.frame.origin.x, y: self.label_market1.frame.origin.y + self.label_market1.frame.size.height + 14 , width: self.label_amount.frame.size.width, height: 21)
        
        self.label_amount1.frame = CGRect(x: self.label_amount1.frame.origin.x, y: self.label_market1.frame.origin.y + self.label_market1.frame.size.height + 14 , width: self.label_amount1.frame.size.width, height: 21)
        
        // from currency
        
        self.label_fromCurrency.frame = CGRect(x: self.label_fromCurrency.frame.origin.x, y: self.label_amount1.frame.origin.y + self.label_amount1.frame.size.height + 14 , width: self.label_fromCurrency.frame.size.width, height: 21)
        
        self.label_fromCurrency1.frame = CGRect(x: self.label_fromCurrency1.frame.origin.x, y: self.label_amount1.frame.origin.y + self.label_amount1.frame.size.height + 14 , width: self.label_fromCurrency1.frame.size.width, height: 21)
        
        // to currency
        
        self.label_toCurrency.frame = CGRect(x: self.label_toCurrency.frame.origin.x, y: self.label_fromCurrency1.frame.origin.y + self.label_fromCurrency1.frame.size.height + 14 , width: self.label_toCurrency.frame.size.width, height: 21)
        
        self.label_toCurrency1.frame = CGRect(x: self.label_toCurrency1.frame.origin.x, y: self.label_fromCurrency1.frame.origin.y + self.label_fromCurrency1.frame.size.height + 14 , width: self.label_toCurrency1.frame.size.width, height: 21)
        
        // description
        
        self.label_description.frame = CGRect(x: self.label_description.frame.origin.x, y: self.label_toCurrency1.frame.origin.y + self.label_toCurrency1.frame.size.height + 14 , width: self.label_description.frame.size.width, height: 21)
        
        self.label_description1.frame = CGRect(x: self.label_description1.frame.origin.x, y: self.label_toCurrency1.frame.origin.y + self.label_toCurrency1.frame.size.height + 14 , width: self.label_description1.frame.size.width, height: 21)
        
        let desc : String = self.label_description1.text!
        let queHeight1 : CGFloat = desc.height(withConstrainedWidth: self.label_description1.frame.size.width, font: UIFont.systemFont(ofSize: 17))
        self.label_description1.frame = CGRect(x: self.label_description1.frame.origin.x, y: self.label_description1.frame.origin.y, width: self.label_description1.frame.size.width, height: queHeight1)
        
        self.myScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.label_description1.frame.size.height + self.label_description1.frame.origin.y + 50)
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func action_chat(_ sender: Any) {
        
        let regisetr : Chat = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! Chat
        regisetr.post_id = exp_id
        regisetr.self_user_id = self_id
        regisetr.post_user_id = post_uid
        regisetr.oherMobileNo = mobileNumber
        regisetr.post_name = post_fname
        
        self.navigationController?.pushViewController(regisetr, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

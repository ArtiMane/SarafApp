//
//  Register1.swift
//  localization
//
//  Created by Developer on 5/12/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class Register1: UIViewController ,UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{

    
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var view_firstName: UIView!
    @IBOutlet weak var label_personalInfo: UILabel!
    @IBOutlet weak var label_signUp: UILabel!
    
    @IBOutlet weak var image_red6: UIImageView!
    @IBOutlet weak var text_cities: UITextField!
    @IBOutlet weak var text_countries: UITextField!
    @IBOutlet weak var text_password: UITextField!
    @IBOutlet weak var text_emailId: UITextField!
    @IBOutlet weak var text_mobileNo: UITextField!
    @IBOutlet weak var text_lasName: UITextField!
    @IBOutlet weak var view_mobileNumber: UIView!
    @IBOutlet weak var view_lastName: UIView!
    
    @IBOutlet weak var text_firstName: UITextField!
    @IBOutlet weak var view_emailId: UIView!
    
    @IBOutlet weak var view_cities: UIView!
    @IBOutlet weak var view_country: UIView!
    @IBOutlet weak var view_password: UIView!
    
    @IBOutlet weak var button_next: UIButton!
    
    @IBOutlet weak var image_red3: UIImageView!
    @IBOutlet weak var image_red2: UIImageView!
    @IBOutlet weak var image_red1: UIImageView!
    
    
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var image_red5: UIImageView!
    @IBOutlet weak var image_red4: UIImageView!
    
    var pickerView = UIPickerView()
    var pickerView1 = UIPickerView()
    
    var arrayLanguage = [String]()
    var arrayCountryId = [String]()
    
    var arrayCities = [String]()
    var arrayCitiesId = [String]()
    var selectedCountryId = ""
    var selectedCityId = "1"
    var selected = 0
    
    
    @IBOutlet weak var image_red7: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view_cities.layer.cornerRadius = 3.0
        self.view_country.layer.cornerRadius = 3.0
        self.view_emailId.layer.cornerRadius = 3.0
        self.view_lastName.layer.cornerRadius = 3.0
        self.view_password.layer.cornerRadius = 3.0
        self.view_firstName.layer.cornerRadius = 3.0
        self.view_mobileNumber.layer.cornerRadius = 3.0
        self.button_next.layer.cornerRadius = 3.0
        
       
        
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        
        self.pickerView1.dataSource = self
        self.pickerView1.delegate = self
        
        self.text_cities.inputView = pickerView1
        self.text_countries.inputView = pickerView
        
        self.text_cities.delegate = self
        self.text_emailId.delegate = self
        self.text_lasName.delegate = self
        self.text_mobileNo.delegate = self
        self.text_password.delegate = self
        self.text_countries.delegate = self
        self.text_firstName.delegate = self
        
        self.text_emailId.returnKeyType = .next
        self.text_mobileNo.returnKeyType = .next
        self.text_firstName.returnKeyType = .next
        self.text_lasName.returnKeyType = .next
        
        self.text_emailId.keyboardType = .emailAddress
        self.text_mobileNo.keyboardType = .numberPad
        
        
        
        self.callCoutries()
    }
    
    override func viewWillLayoutSubviews() {
        self.myScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 735)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
    }
    
    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if((UserDefaults.standard.value(forKey: "ValuesExist")) != nil){
            
            self.text_countries.text = UserDefaults.standard.value(forKey: "Country") as? String
            self.text_cities.text = UserDefaults.standard.value(forKey: "City") as? String
            self.text_password.text = UserDefaults.standard.value(forKey: "Password") as? String
            self.text_emailId.text = UserDefaults.standard.value(forKey: "EmailId") as? String
            self.text_mobileNo.text = UserDefaults.standard.value(forKey: "MobileNo") as? String
            self.text_lasName.text = UserDefaults.standard.value(forKey: "LastName") as? String
            self.text_firstName.text = UserDefaults.standard.value(forKey: "FirstName") as? String
        }
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            
            self.myScrollView.semanticContentAttribute = .forceLeftToRight
            self.view_cities.semanticContentAttribute = .forceLeftToRight
            self.view_country.semanticContentAttribute = .forceLeftToRight
            self.view_emailId.semanticContentAttribute = .forceLeftToRight
            self.view_lastName.semanticContentAttribute = .forceLeftToRight
            self.view_password.semanticContentAttribute = .forceLeftToRight
            self.view_firstName.semanticContentAttribute = .forceLeftToRight
            self.view_mobileNumber.semanticContentAttribute = .forceLeftToRight
            
            self.text_cities.textAlignment = .left
            self.text_emailId.textAlignment = .left
            self.text_lasName.textAlignment = .left
            self.text_mobileNo.textAlignment = .left
            self.text_password.textAlignment = .left
            self.text_countries.textAlignment = .left
            self.text_firstName.textAlignment = .left
            
            self.label_signUp.textAlignment = .left
            self.label_personalInfo.textAlignment = .left
            
            self.buttonBack.setImage(UIImage(named : "leftArrow.png"), for: .normal)
            
            self.label_signUp.text = ("Sign Up".localizeStringUsingSystemLang)
            self.label_personalInfo.text = ("Personal Information".localizeStringUsingSystemLang)
            
            self.text_cities.attributedPlaceholder = NSAttributedString(string: "City".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_emailId.attributedPlaceholder = NSAttributedString(string: "Email Id".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_lasName.attributedPlaceholder = NSAttributedString(string: "Last Name".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_mobileNo.attributedPlaceholder = NSAttributedString(string: "Mobile No".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_password.attributedPlaceholder = NSAttributedString(string: "Password".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_countries.attributedPlaceholder = NSAttributedString(string: "Country".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_firstName.attributedPlaceholder = NSAttributedString(string: "First Name".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            self.button_next.setTitle("Next".localizeStringUsingSystemLang, for: .normal)
            
            
        }
        else if(language == "دری"){
            
            self.myScrollView.semanticContentAttribute = .forceRightToLeft
            self.view_cities.semanticContentAttribute = .forceRightToLeft
            self.view_country.semanticContentAttribute = .forceRightToLeft
            self.view_emailId.semanticContentAttribute = .forceRightToLeft
            self.view_lastName.semanticContentAttribute = .forceRightToLeft
            self.view_password.semanticContentAttribute = .forceRightToLeft
            self.view_firstName.semanticContentAttribute = .forceRightToLeft
            self.view_mobileNumber.semanticContentAttribute = .forceRightToLeft
            
            self.text_cities.textAlignment = .right
            self.text_emailId.textAlignment = .right
            self.text_lasName.textAlignment = .right
            self.text_mobileNo.textAlignment = .right
            self.text_password.textAlignment = .right
            self.text_countries.textAlignment = .right
            self.text_firstName.textAlignment = .right
            
            self.label_signUp.textAlignment = .right
            self.label_personalInfo.textAlignment = .right
            
            self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
            
            self.label_signUp.text = ("Sign Up".localizeString1(lang: "fa-AF"))
            self.label_personalInfo.text = ("Personal Information".localizeString1(lang: "fa-AF"))
            
            self.text_cities.attributedPlaceholder = NSAttributedString(string: "City".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_emailId.attributedPlaceholder = NSAttributedString(string: "Email Id".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_lasName.attributedPlaceholder = NSAttributedString(string: "Last Name".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_mobileNo.attributedPlaceholder = NSAttributedString(string: "Mobile No".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_password.attributedPlaceholder = NSAttributedString(string: "Password".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_countries.attributedPlaceholder = NSAttributedString(string: "Country".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_firstName.attributedPlaceholder = NSAttributedString(string: "First Name".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            self.button_next.setTitle("Next".localizeString1(lang: "fa-AF"), for: .normal)
        }
        else {
            self.myScrollView.semanticContentAttribute = .forceRightToLeft
            self.view_cities.semanticContentAttribute = .forceRightToLeft
            self.view_country.semanticContentAttribute = .forceRightToLeft
            self.view_emailId.semanticContentAttribute = .forceRightToLeft
            self.view_lastName.semanticContentAttribute = .forceRightToLeft
            self.view_password.semanticContentAttribute = .forceRightToLeft
            self.view_firstName.semanticContentAttribute = .forceRightToLeft
            self.view_mobileNumber.semanticContentAttribute = .forceRightToLeft
            
            self.text_cities.textAlignment = .right
            self.text_emailId.textAlignment = .right
            self.text_lasName.textAlignment = .right
            self.text_mobileNo.textAlignment = .right
            self.text_password.textAlignment = .right
            self.text_countries.textAlignment = .right
            self.text_firstName.textAlignment = .right
            
            self.label_signUp.textAlignment = .right
            self.label_personalInfo.textAlignment = .right
            
            self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
            
            self.label_signUp.text = ("Sign Up".localizeString1(lang: "ps-AF"))
            self.label_personalInfo.text = ("Personal Information".localizeString1(lang: "ps-AF"))
            
            self.text_cities.attributedPlaceholder = NSAttributedString(string: "City".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_emailId.attributedPlaceholder = NSAttributedString(string: "Email Id".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_lasName.attributedPlaceholder = NSAttributedString(string: "Last Name".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_mobileNo.attributedPlaceholder = NSAttributedString(string: "Mobile No".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_password.attributedPlaceholder = NSAttributedString(string: "Password".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_countries.attributedPlaceholder = NSAttributedString(string: "Country".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_firstName.attributedPlaceholder = NSAttributedString(string: "First Name".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            self.button_next.setTitle("Next".localizeString1(lang: "ps-AF"), for: .normal)
        }
    }
    
    //MARK: - Next Button

    @IBAction func action_next(_ sender: Any) {
        
        if(checkValidation() == false){
            
        }
        else if(!(self.isValidEmail(testStr: self.text_emailId.text!))){
          
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please enter valid email address.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            UserDefaults.standard.set("ValuesExist", forKey: "ValuesExist")
            UserDefaults.standard.set(self.text_cities.text, forKey: "City")
            UserDefaults.standard.set(selectedCityId, forKey: "CityId")
            UserDefaults.standard.set(selectedCountryId, forKey: "CountryId")
            UserDefaults.standard.set(self.text_countries.text, forKey: "Country")
            UserDefaults.standard.set(self.text_firstName.text, forKey: "FirstName")
            UserDefaults.standard.set(self.text_password.text, forKey: "Password")
            UserDefaults.standard.set(self.text_emailId.text, forKey: "EmailId")
            UserDefaults.standard.set(self.text_mobileNo.text, forKey: "MobileNo")
            UserDefaults.standard.set(self.text_lasName.text, forKey: "LastName")
            
            let regisetr : Register2 = self.storyboard?.instantiateViewController(withIdentifier: "Register2") as! Register2
            self.navigationController?.pushViewController(regisetr, animated: true)
        }
    }
    
    func checkValidation() -> Bool{
        
        var value : Bool = false
        var count = 0
        if(self.text_firstName.text == ""){
           image_red1.isHidden = false
            value = false
            
        }
        else {
           image_red1.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_lasName.text == ""){
           image_red2.isHidden = false
            value = false
            
        }
        else {
            image_red2.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_mobileNo.text == ""){
          image_red3.isHidden = false
            value = false
        }
        else {
            image_red3.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_emailId.text == ""){
          image_red4.isHidden = false
            value = false
        }
        else {
           image_red4.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_password.text == ""){
          image_red5.isHidden = false
            value = false
        }
        else {
           image_red5.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_countries.text == ""){
           image_red6.isHidden = false
            value = false
        }
        else {
          image_red6.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_cities.text == ""){
           image_red7.isHidden = false
            value = false
        }
        else {
           image_red7.isHidden = true
            value = true
            count = count + 1
        }
        if(count == 7){
            return true
        }
        else {
            return false}
    }
    
    //MARK: - Call APIs
    func callCoutries(){
       
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            loadingNotification.label.text = "Fetching countries"
            
            
            Alamofire.request("http://qtademo.com/saraf/api/countries", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let arrayLanguage : NSArray = json["countries"] as! NSArray
                    
                    for var i in 0..<arrayLanguage.count{
                        
                        let language = (arrayLanguage[i] as! NSDictionary).value(forKey: "c_name")
                        self.arrayLanguage.append(language as! String)
                        
                        let languageId = (arrayLanguage[i] as! NSDictionary).value(forKey: "c_id")
                        self.arrayCountryId.append(languageId as! String)
                        
                        
                    }
                    self.pickerView.reloadAllComponents()
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func callCities(){
        
        selected = 1
        
        self.arrayCitiesId.removeAll()
        self.arrayCities.removeAll()
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            loadingNotification.label.text = "Fetching Cities"
            
            
            Alamofire.request("http://qtademo.com/saraf/api/cities", method: .post, parameters: ["c_id" : selectedCountryId], encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let arrayLanguage : NSArray = json["cities"] as! NSArray
                    print(json)
                     for var i in 0..<arrayLanguage.count{
                        
                        let language = (arrayLanguage[i] as! NSDictionary).value(forKey: "ct_name")
                        self.arrayCities.append(language as! String)
                        
                        let languageId = (arrayLanguage[i] as! NSDictionary).value(forKey: "ct_id")
                        self.arrayCitiesId.append(languageId as! String)
                        
                        
                    }
                    self.pickerView1.reloadAllComponents()
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    //MARK: - Pickerview method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == self.pickerView){
            return arrayLanguage.count
        }
        else {
           return arrayCities.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == self.pickerView){
            return arrayLanguage[row]
        }
        else {
            return arrayCities[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == self.pickerView){
            self.text_countries.text = arrayLanguage[row]
            image_red6.isHidden = true
            selectedCountryId = arrayCountryId[row]
           //  UserDefaults.standard.set(selectedCityId, forKey: "CityId")
            self.text_cities.text = ""
            self.callCities()
        }
        else {
            image_red7.isHidden = true
            if(arrayCities.count == 0){
                
            }
            else {
                self.text_cities.text = arrayCities[row]
                selectedCityId = arrayCitiesId[row]
            }
            
            
        }
    }
    
    //MARK: - Text field delegate method
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == self.text_firstName){
            self.image_red1.isHidden = true
        }
        if(textField == self.text_lasName){
            self.image_red2.isHidden = true
        }
        if(textField == self.text_mobileNo){
            self.image_red3.isHidden = true
        }
        if(textField == self.text_emailId){
            self.image_red4.isHidden = true
        }
        if(textField == self.text_password){
            self.image_red5.isHidden = true
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.text_firstName){
           self.text_lasName.becomeFirstResponder()
        }
        if(textField == self.text_lasName){
            self.text_mobileNo.becomeFirstResponder()
        }
        if(textField == self.text_mobileNo){
            self.text_emailId.becomeFirstResponder()
        }
        if(textField == self.text_emailId){
            self.text_password.becomeFirstResponder()
        }
        if(textField == self.text_password){
            textField.resignFirstResponder()
        }
        return true
    }
    
    //MARK: - Check email validation
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

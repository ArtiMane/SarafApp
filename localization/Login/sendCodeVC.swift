//
//  sendCodeVC.swift
//  localization
//
//  Created by Developer on 5/1/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class sendCodeVC: UIViewController {

    @IBOutlet weak var textMobileNumber: UITextField!
    
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var viewSuper: UIView!
    @IBOutlet weak var buttonGetCode: UIButton!
    @IBOutlet weak var viewMobile: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        textMobileNumber.attributedPlaceholder = NSAttributedString(string: "Mobile Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        buttonGetCode.layer.cornerRadius = 3.0
        viewMobile.layer.cornerRadius = 3.0
    }
    
    @IBAction func action_sendCode(_ sender: Any) {
        
        let sendCode : SetCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "SetCodeVC") as! SetCodeVC
        self.navigationController?.pushViewController(sendCode, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(UserDefaults.standard.value(forKey: "selectedLanguage") as! String != "English"){
            
            self.viewMobile.semanticContentAttribute = .forceRightToLeft
            self.viewSuper.semanticContentAttribute = .forceRightToLeft
            self.textMobileNumber.textAlignment  = .right
            self.buttonBack.setImage(UIImage(named:"rightArrow.png"), for: .normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

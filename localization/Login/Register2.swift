//
//  Register2.swift
//  localization
//
//  Created by Developer on 5/12/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class Register2: UIViewController  , UIPickerViewDataSource , UITextFieldDelegate ,UIPickerViewDelegate{

    
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var button_register: UIButton!
    @IBOutlet weak var view_licenceDate: UIView!
    @IBOutlet weak var view_licenceNo: UIView!
    @IBOutlet weak var view_mobile: UIView!
    @IBOutlet weak var view_storeName: UIView!
    @IBOutlet weak var view_market: UIView!
    @IBOutlet weak var view_sarafName: UIView!
    @IBOutlet weak var label_accountInfo: UILabel!
    @IBOutlet weak var label_signUp: UILabel!
    
    @IBOutlet weak var text_saafiName: UITextField!
    
    @IBOutlet weak var text_date: UITextField!
    @IBOutlet weak var text_licenceNo: UITextField!
    @IBOutlet weak var text_mob: UITextField!
    @IBOutlet weak var text_storename: UITextField!
    @IBOutlet weak var text_market: UITextField!
    
    
    @IBOutlet weak var image_red1: UIImageView!
    
    @IBOutlet weak var image_red6: UIImageView!
    @IBOutlet weak var image_red5: UIImageView!
    @IBOutlet weak var image_red4: UIImageView!
    @IBOutlet weak var image_red3: UIImageView!
    @IBOutlet weak var image_red2: UIImageView!
    
    var datePicker : UIDatePicker = UIDatePicker()
    var toolbarView : UIView = UIView()
    var toolbarButton : UIButton = UIButton()
    var pickerView = UIPickerView()
    var arrayLanguage = [String]()
    var arraySelectedMarketId = [String]()
    var selectedMarketId = ""
    var dob = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: self.view.frame.size.height - 200, width: self.view.frame.size.width, height: 200))
        datePicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        datePicker.datePickerMode = .date
        toolbarView = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height - 250, width: self.view.frame.size.width, height: 50))
        toolbarButton = UIButton(frame: CGRect(x: self.view.frame.size.width - 110, y: 10, width: 100, height: 30))
        toolbarButton.setTitle("Done", for: .normal)
        toolbarButton.setTitleColor(UIColor.blue, for: .normal)
        toolbarButton.addTarget(self, action: #selector(closePickerView(sender:)), for: .touchUpInside)
        toolbarView.backgroundColor = UIColor.white
        toolbarView.addSubview(toolbarButton)
        self.myScrollView.addSubview(toolbarView)
        self.myScrollView.addSubview(datePicker)
        datePicker.isHidden = true
        toolbarView.isHidden = true
        datePicker.backgroundColor = UIColor.white
     //   datePicker.maximumDate = Date()
      //  text_date.inputView = datePicker
        
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        self.text_market.inputView = pickerView
        
        self.view_licenceDate.layer.cornerRadius = 3.0
        self.view_storeName.layer.cornerRadius = 3.0
        self.view_sarafName.layer.cornerRadius = 3.0
        self.view_licenceNo.layer.cornerRadius = 3.0
        self.view_mobile.layer.cornerRadius = 3.0
        self.view_market.layer.cornerRadius = 3.0
        self.button_register.layer.cornerRadius = 3.0
        
        self.text_storename.delegate = self
        self.text_saafiName.delegate = self
        self.text_licenceNo.delegate = self
        self.text_market.delegate = self
        self.text_date.delegate = self
        self.text_mob.delegate = self
        
        
        self.text_saafiName.returnKeyType = .next
        self.text_storename.returnKeyType = .next
        self.text_licenceNo.returnKeyType = .next
        self.text_market.returnKeyType = .next
        self.text_mob.returnKeyType = .next
        
       
        self.text_mob.keyboardType = .numberPad
        
        self.callMarket()
    }
    
    //MARK: - Text field delegate method
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        datePicker.isHidden = true
        toolbarView.isHidden = true
        if(textField == self.text_saafiName){
            self.image_red1.isHidden = true
        }
        if(textField == self.text_market){
            self.image_red2.isHidden = true
        }
        if(textField == self.text_storename){
            self.image_red3.isHidden = true
        }
        if(textField == self.text_mob){
            self.image_red4.isHidden = true
        }
        if(textField == self.text_licenceNo){
            self.image_red5.isHidden = true
        }
        if(textField == self.text_date){
            self.image_red6.isHidden = true
        }
        if(textField == self.text_date){
            datePicker.isHidden = false
            toolbarView.isHidden = false
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.text_saafiName){
            self.text_market.becomeFirstResponder()
        }
        if(textField == self.text_market){
            self.text_storename.becomeFirstResponder()
        }
        if(textField == self.text_storename){
            self.text_mob.becomeFirstResponder()
        }
        if(textField == self.text_mob){
            self.text_licenceNo.becomeFirstResponder()
        }
        if(textField == self.text_licenceNo){
            text_date.becomeFirstResponder()
        }
        if(textField == self.text_date){
            text_date.resignFirstResponder()
        }
        return true
    }
    
    
    @objc func closePickerView(sender: UIButton) {
        datePicker.isHidden = true
        toolbarView.isHidden = true
        //   self.myScrollView.isScrollEnabled = false
        self.myScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.myScrollView.isScrollEnabled = false
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        text_date.text = dateFormatter.string(from: datePicker.date)
        dob = dateFormatter1.string(from: datePicker.date)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
    }
    
    override func viewWillLayoutSubviews() {
        self.myScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 700)
    }
    
    @IBAction func action_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            
            self.myScrollView.semanticContentAttribute = .forceLeftToRight
            self.view_market.semanticContentAttribute = .forceLeftToRight
            self.view_mobile.semanticContentAttribute = .forceLeftToRight
            self.view_licenceNo.semanticContentAttribute = .forceLeftToRight
            self.view_sarafName.semanticContentAttribute = .forceLeftToRight
            self.view_storeName.semanticContentAttribute = .forceLeftToRight
            self.view_licenceDate.semanticContentAttribute = .forceLeftToRight
           
            
            self.text_mob.textAlignment = .left
            self.text_date.textAlignment = .left
            self.text_market.textAlignment = .left
            self.text_licenceNo.textAlignment = .left
            self.text_saafiName.textAlignment = .left
            self.text_storename.textAlignment = .left
           
            
            self.label_signUp.textAlignment = .left
            self.label_accountInfo.textAlignment = .left
            
            self.buttonBack.setImage(UIImage(named : "leftArrow.png"), for: .normal)
            
            self.label_signUp.text = ("Sign Up".localizeStringUsingSystemLang)
            self.label_accountInfo.text = ("Account Information".localizeStringUsingSystemLang)
            
            self.text_storename.attributedPlaceholder = NSAttributedString(string: "Shop Number".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_saafiName.attributedPlaceholder = NSAttributedString(string: "Sarafi Name".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_licenceNo.attributedPlaceholder = NSAttributedString(string: "License Number".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_market.attributedPlaceholder = NSAttributedString(string: "Exchange Market".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_date.attributedPlaceholder = NSAttributedString(string: "License Expiry Date".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_mob.attributedPlaceholder = NSAttributedString(string: "Office Phone Number".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
           
            
            self.button_register.setTitle("Register".localizeStringUsingSystemLang, for: .normal)
            
            
        }
        else if(language == "دری"){
            
            self.myScrollView.semanticContentAttribute = .forceRightToLeft
            self.myScrollView.semanticContentAttribute = .forceRightToLeft
            self.view_market.semanticContentAttribute = .forceRightToLeft
            self.view_mobile.semanticContentAttribute = .forceRightToLeft
            self.view_licenceNo.semanticContentAttribute = .forceRightToLeft
            self.view_sarafName.semanticContentAttribute = .forceRightToLeft
            self.view_storeName.semanticContentAttribute = .forceRightToLeft
            self.view_licenceDate.semanticContentAttribute = .forceRightToLeft
            
            
            self.text_mob.textAlignment = .right
            self.text_date.textAlignment = .right
            self.text_market.textAlignment = .right
            self.text_licenceNo.textAlignment = .right
            self.text_saafiName.textAlignment = .right
            self.text_storename.textAlignment = .right
            
            self.label_signUp.textAlignment = .right
            self.label_accountInfo.textAlignment = .right
            
            self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
            
            self.label_signUp.text = ("Sign Up".localizeString1(lang: "fa-AF"))
            self.label_accountInfo.text = ("Account Information".localizeString1(lang: "fa-AF"))
            
            
            
            self.text_storename.attributedPlaceholder = NSAttributedString(string: "Shop Number".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_saafiName.attributedPlaceholder = NSAttributedString(string: "Sarafi Name".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_licenceNo.attributedPlaceholder = NSAttributedString(string: "License Number".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_market.attributedPlaceholder = NSAttributedString(string: "Exchange Market".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_date.attributedPlaceholder = NSAttributedString(string: "License Expiry Date".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_mob.attributedPlaceholder = NSAttributedString(string: "Office Phone Number".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            self.button_register.setTitle("Register".localizeString1(lang: "fa-AF"), for: .normal)
        }
        else {
            self.myScrollView.semanticContentAttribute = .forceRightToLeft
            self.myScrollView.semanticContentAttribute = .forceRightToLeft
            self.myScrollView.semanticContentAttribute = .forceRightToLeft
            self.view_market.semanticContentAttribute = .forceRightToLeft
            self.view_mobile.semanticContentAttribute = .forceRightToLeft
            self.view_licenceNo.semanticContentAttribute = .forceRightToLeft
            self.view_sarafName.semanticContentAttribute = .forceRightToLeft
            self.view_storeName.semanticContentAttribute = .forceRightToLeft
            self.view_licenceDate.semanticContentAttribute = .forceRightToLeft
            
            
            self.text_mob.textAlignment = .right
            self.text_date.textAlignment = .right
            self.text_market.textAlignment = .right
            self.text_licenceNo.textAlignment = .right
            self.text_saafiName.textAlignment = .right
            self.text_storename.textAlignment = .right
            
            self.label_signUp.textAlignment = .right
            self.label_accountInfo.textAlignment = .right
            
            self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
            
            self.label_signUp.text = ("Sign Up".localizeString1(lang: "ps-AF"))
            self.label_accountInfo.text = ("Account Information".localizeString1(lang: "ps-AF"))
            
            
            self.text_storename.attributedPlaceholder = NSAttributedString(string: "Shop Number".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_saafiName.attributedPlaceholder = NSAttributedString(string: "Sarafi Name".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_licenceNo.attributedPlaceholder = NSAttributedString(string: "License Number".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_market.attributedPlaceholder = NSAttributedString(string: "Exchange Market".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_date.attributedPlaceholder = NSAttributedString(string: "License Expiry Date".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_mob.attributedPlaceholder = NSAttributedString(string: "Office Phone Number".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            self.button_register.setTitle("Register".localizeString1(lang: "ps-AF"), for: .normal)
        }
    }
    
    //MARK: - Register

    @IBAction func action_register(_ sender: Any) {
        
        if(checkValidation() == false){
            
        }
        else {
            
            if CheckConnection.isConnectedToNetwork() {
                print("Connected")
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = .indeterminate
                loadingNotification.label.text = "Registering"
                
                print(selectedMarketId)
                print(self.text_saafiName.text)
                print(self.text_storename.text)
                print(UserDefaults.standard.value(forKey: "FirstName")!)
                 print(UserDefaults.standard.value(forKey: "LastName")!)
                 print(UserDefaults.standard.value(forKey: "MobileNo")!)
                 print(UserDefaults.standard.value(forKey: "EmailId")!)
                 print(UserDefaults.standard.value(forKey: "CityId")!)
                 print(UserDefaults.standard.value(forKey: "CountryId")!)
                 print(UserDefaults.standard.value(forKey: "lid")!)
                 
                
                Alamofire.request("http://qtademo.com/saraf/api/register", method: .post, parameters: ["fn" : UserDefaults.standard.value(forKey: "FirstName")! , "ln" : UserDefaults.standard.value(forKey: "LastName")! , "m" : UserDefaults.standard.value(forKey: "MobileNo")! , "e" : UserDefaults.standard.value(forKey: "EmailId")!, "ct_id" : UserDefaults.standard.value(forKey: "CityId")!, "c_id" : UserDefaults.standard.value(forKey: "CountryId")!, "exm_id" : selectedMarketId, "lid" : UserDefaults.standard.value(forKey: "lid")!, "s" : self.text_saafiName.text!, "rln" : self.text_saafiName.text!, "on" : self.text_storename.text!,
                                                                                                       "op" : self.text_mob.text!,"lin" : self.text_licenceNo.text! , "l_date" : dob, "p" :UserDefaults.standard.value(forKey: "Password")!, ], encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    switch response.result {
                        
                      
                    case .success:
                        
                        
                        print(response)
                        
                        let json : [String : Any] = response.result.value as! [String : Any]
                        let code :Int = json["code"] as! Int
                        
                        if(code == 200){
                            
                            let uid : Int = json["uid"] as! Int
                            let uid_string : String = String(uid)
                            UserDefaults.standard.set(json["uid"], forKey: "uid")
                             UserDefaults.standard.set(json["ct_id"], forKey: "ct_id")
                            let lid = json["lid"] as! String
                            var selectedLanguageId = ""
                            if(lid == "1"){
                                selectedLanguageId = "English"
                            }
                            if(lid == "2"){
                                selectedLanguageId = "دری"
                            }
                            if(lid == "3"){
                                selectedLanguageId = "pasto"
                            }
                            
                            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                            loadingNotification.mode = .indeterminate
                            loadingNotification.label.text = "Registering"
                            
                            User.registerUser(withFname: UserDefaults.standard.value(forKey: "FirstName") as! String, lname: UserDefaults.standard.value(forKey: "LastName") as! String, mobile: UserDefaults.standard.value(forKey: "MobileNo") as! String, sarafi_name: self.text_saafiName.text!, uid: uid_string, email: UserDefaults.standard.value(forKey: "EmailId") as! String, password: UserDefaults.standard.value(forKey: "Password") as! String, profilePic: UIImage(named:"profile.png")!, completion: { (status) in
                                
                                MBProgressHUD.hide(for: self.view, animated: true)
                                
                                if(status == true){
                                    
                                    print("registered successfully")
                                }
                                else {
                                    print("not registered")
                                }
                            })
                            
                            
                           UserDefaults.standard.set(json["c_id"], forKey: "c_id")
                            UserDefaults.standard.set(json["exm_id"], forKey: "exm_id")
                             UserDefaults.standard.set(json["exm_id"], forKey: "exm_id")
                             UserDefaults.standard.set(json["fname"], forKey: "fname")
                            UserDefaults.standard.set(json["lname"], forKey: "lname")
                           UserDefaults.standard.set("user", forKey: "LoginType")
                            
                            UserDefaults.standard.removeObject(forKey: "ValuesExist")
                             UserDefaults.standard.removeObject(forKey: "City")
                             UserDefaults.standard.removeObject(forKey: "CityId")
                             UserDefaults.standard.removeObject(forKey: "CountryId")
                             UserDefaults.standard.removeObject(forKey: "Country")
                             UserDefaults.standard.removeObject(forKey: "FirstName")
                           //  UserDefaults.standard.removeObject(forKey: "Password")
                             UserDefaults.standard.removeObject(forKey: "EmailId")
                             UserDefaults.standard.removeObject(forKey: "LastName")
                          //  UserDefaults.standard.removeObject(forKey: "MobileNo")
                           
                            self.navigationController?.popToRootViewController(animated: true)
                            
                          //  UserDefaults.standard.set(selectedLanguageId, forKey: "AppLanguageKey")
//                            let regisetr : VerifyVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyVC") as! VerifyVC
//                            regisetr.verificationCode = json["varification_code_code"] as! String
//                            self.navigationController?.pushViewController(regisetr, animated: true)
//                            let regisetr : tabbarController = self.storyboard?.instantiateViewController(withIdentifier: "tabbarController") as! tabbarController
//                            self.navigationController?.pushViewController(regisetr, animated: true)
                            
                            
                            
                        
                           
                            
                           
//                            User.registerUser(withName: name, email: UserDefaults.standard.value(forKey: "EmailId") as! String, password: UserDefaults.standard.value(forKey: "Password") as! String, profilePic: UIImage(named:"profile.png")!, completion: { (status) in
//                                if(status == true){
//
//                                    print("registered successfully")
//                                }
//                                else {
//                                    print("not registered")
//                                }
//                            })
                            
                            let alert : UIAlertController = UIAlertController(title: "Success", message: "You are registered successfully.", preferredStyle: .actionSheet)
                            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                                let regisetr : UserLogin = self.storyboard?.instantiateViewController(withIdentifier: "UserLogin") as! UserLogin
                                self.navigationController?.pushViewController(regisetr, animated: true)
                                alert.dismiss(animated: true, completion: nil)
                            })
                            alert.addAction(actionSheet)
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else {
                            let alert : UIAlertController = UIAlertController(title: "Error", message: "Problem occured while registering your profile. Please try again later.", preferredStyle: .actionSheet)
                            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                                alert.dismiss(animated: true, completion: nil)
                            })
                            alert.addAction(actionSheet)
                            self.present(alert, animated: true, completion: nil)
                        }

                        
                        break
                    case .failure(let error):
                        
                        print(error)
                        let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(actionSheet)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                })
                
            }
            else{
                let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
                let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                })
                alert.addAction(actionSheet)
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        
    }
    
    func checkValidation() -> Bool{
        
        var value : Bool = false
        var count = 0
        if(self.text_saafiName.text == ""){
            image_red1.isHidden = false
            value = false
            
        }
        else {
            image_red1.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_market.text == ""){
            image_red2.isHidden = false
            value = false
            
        }
        else {
            image_red2.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_storename.text == ""){
            image_red3.isHidden = false
            value = false
        }
        else {
            image_red3.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_mob.text == ""){
            image_red4.isHidden = false
            value = false
        }
        else {
            image_red4.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_licenceNo.text == ""){
            image_red5.isHidden = false
            value = false
        }
        else {
            image_red5.isHidden = true
            value = true
            count = count + 1
        }
        if(self.text_date.text == ""){
            image_red6.isHidden = false
            value = false
        }
        else {
            image_red6.isHidden = true
            value = true
            count = count + 1
        }
        
        if(count == 6){
            return true
        }
        else {
            return false}
    }
    
    
    //MARK: - Pickerview method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
            return arrayLanguage.count
        
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
            return arrayLanguage[row]
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(arrayLanguage.count == 0){}
        else {
            self.text_market.text = arrayLanguage[row]
            image_red6.isHidden = true
            selectedMarketId = arraySelectedMarketId[row]
        }
        
    }
    
    // MARK: - FETCH market api
    
    func callMarket(){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
           // loadingNotification.label.text = "Fetching countries"
            
            
            Alamofire.request("http://qtademo.com/saraf/api/exm_list", method: .post, parameters: ["ct_id" : UserDefaults.standard.value(forKey: "CityId")!], encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let arrayLanguage : NSArray = json["exm_list"] as! NSArray
                    
                    for var i in 0..<arrayLanguage.count{
                        
                        let language = (arrayLanguage[i] as! NSDictionary).value(forKey: "exm_name")
                        self.arrayLanguage.append(language as! String)
                        
                        let id1 = (arrayLanguage[i] as! NSDictionary).value(forKey: "exm_id")
                        self.arraySelectedMarketId.append(id1 as! String)
                        
                        
                        
                        
                    }
                    self.pickerView.reloadAllComponents()
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

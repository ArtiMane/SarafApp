//
//  LoginVC.swift
//  localization
//
//  Created by Developer on 4/30/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class LoginVC: UIViewController , UIPickerViewDataSource , UIPickerViewDelegate {

    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var viewSuper: UIView!
    
    @IBOutlet weak var textRegister: UITextField!
    @IBOutlet weak var viewRegister: UIView!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var textMobile: UITextField!
    var arrayUserType = [String]()
    var pickerView = UIPickerView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       // viewSuper.semanticContentAttribute = .forceRightToLeft
        viewMobile.layer.cornerRadius = 3.0
        viewPassword.layer.cornerRadius = 3.0
     //   viewRegister.layer.cornerRadius = 3.0
        buttonLogin.layer.cornerRadius = 3.0
        
        arrayUserType = ["Admin","Agent","Money Transfer Service Provider"]
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        
        textMobile.attributedPlaceholder = NSAttributedString(string: "Mobile Number",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
        textPassword.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
      //  textRegister.inputView = pickerView
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(UserDefaults.standard.value(forKey: "selectedLanguage") as! String != "English"){
            
            self.viewSuper.semanticContentAttribute = .forceRightToLeft
          //  self.viewRegister.semanticContentAttribute = .forceRightToLeft
            self.viewPassword.semanticContentAttribute = .forceRightToLeft
            self.viewMobile.semanticContentAttribute = .forceRightToLeft
            
            self.textMobile.textAlignment  = .right
            self.textPassword.textAlignment = .right
          //  self.textRegister.textAlignment = .right
            
          //  let scroll = UIScrollView()
            
        }
    }
    
    @IBAction func action_login(_ sender: Any) {
    }
    
    @IBAction func action_forgetPassword(_ sender: Any) {
        
        let code : sendCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "sendCodeVC") as! sendCodeVC
        self.navigationController?.pushViewController(code, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_proceed(_ sender: Any) {
       
        let register : RegisterProvider = self.storyboard?.instantiateViewController(withIdentifier: "RegisterProvider") as! RegisterProvider
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Pickerview method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayUserType.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayUserType[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textRegister.text = arrayUserType[row]
        
        
        //self.view.endEditing(true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

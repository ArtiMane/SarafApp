//
//  HistoryVC.swift
//  localization
//
//  Created by Developer on 5/13/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class HistoryVC: UIViewController , UITableViewDelegate , UITableViewDataSource {

    
    
    @IBOutlet weak var view_upper: UIView!
    @IBOutlet weak var label_todays: UILabel!
    @IBOutlet weak var label_ratesHistory: UILabel!
    @IBOutlet weak var label_from: UILabel!
    @IBOutlet weak var label_to: UILabel!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var view_super: UIView!
    @IBOutlet weak var tableView: UITableView!
    var exm_i = ""
    var arrayMarket = NSMutableArray()
    var to = ""
    var from = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        label_to.text = to
        label_from.text = from
        print(exm_i)
        self.callHistory()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            self.tableView.semanticContentAttribute = .forceLeftToRight
            self.view_super.semanticContentAttribute = .forceLeftToRight
            self.view_upper.semanticContentAttribute = .forceLeftToRight
            self.label_ratesHistory.text = ("Rates History".localizeStringUsingSystemLang)
            self.label_todays.text = ("Today".localizeStringUsingSystemLang)
             self.buttonBack.setImage(UIImage(named : "leftArrow.png"), for: .normal)
        }
        else if(language == "دری"){
            self.tableView.semanticContentAttribute = .forceRightToLeft
            self.view_super.semanticContentAttribute = .forceRightToLeft
            self.view_upper.semanticContentAttribute = .forceRightToLeft
            
            self.label_ratesHistory.text = ("Rates History".localizeString1(lang: "fa-AF"))
            self.label_todays.text = ("Today".localizeString1(lang: "fa-AF"))
             self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
        }
        else {
            self.tableView.semanticContentAttribute = .forceRightToLeft
            self.view_super.semanticContentAttribute = .forceRightToLeft
            self.view_upper.semanticContentAttribute = .forceRightToLeft
            
            self.label_ratesHistory.text = ("Rates History".localizeString1(lang: "ps-AF"))
            self.label_todays.text = ("Today".localizeString1(lang: "ps-AF"))
             self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
        }
    }
    
    func callHistory(){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            Alamofire.request("http://qtademo.com/saraf/api/exm_rate_history", method: .post , parameters: ["exmr_id" : exm_i], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        if( json["data"] as? NSDictionary != nil){
                        let arraysqw = (json["data"] as! NSDictionary)
                            self.arrayMarket = (arraysqw.value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                            print(self.arrayMarket)
                        self.tableView.reloadData()
                        }
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMarket.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : HistoryCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! HistoryCell
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            cell.superView.semanticContentAttribute = .forceLeftToRight
            cell.label_buyValue.textAlignment = .left
            cell.label_sellValue.textAlignment = .left
            cell.label_buy1.text = ("SELL".localizeStringUsingSystemLang)
            cell.label_sell1.text = ("SELL".localizeStringUsingSystemLang)
            
        }
        else if(language == "دری"){
            
            cell.superView.semanticContentAttribute = .forceRightToLeft
            cell.label_buyValue.textAlignment = .right
            cell.label_sellValue.textAlignment = .right
            cell.label_buy1.text = ("SELL".localizeString1(lang: "fa-AF"))
            cell.label_sell1.text = ("SELL".localizeString1(lang: "fa-AF"))
        }
        else {
            cell.superView.semanticContentAttribute = .forceRightToLeft
            cell.label_buyValue.textAlignment = .right
            cell.label_sellValue.textAlignment = .right
            cell.label_buy1.text = ("SELL".localizeString1(lang: "ps-AF"))
            cell.label_sell1.text = ("SELL".localizeString1(lang: "ps-AF"))
        }
        
//        let myarray : NSArray = arrayMarket.obj
        let buyValue = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "buy_rate") as! String
        cell.label_buyValue.text = String(describing: Double(buyValue)!.rounded(toPlaces: 2))
        
        
        let cellValue = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "sell_rate") as! String
        cell.label_sellValue.text = String(describing: Double(cellValue)!.rounded(toPlaces: 2))
//        cell.label_buy.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "from_code") as? String
//        cell.label_sell.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "to_code") as? String
        cell.label_date.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "date") as? String
        cell.superView.layer.cornerRadius = 3.0
      

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 81.0
    }
    
    

    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

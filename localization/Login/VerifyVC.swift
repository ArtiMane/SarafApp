//
//  VerifyVC.swift
//  localization
//
//  Created by Developer on 5/14/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class VerifyVC: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var label_verify: UILabel!
    
    @IBOutlet weak var image_red1: UIImageView!
    @IBOutlet weak var button_verify: UIButton!
    @IBOutlet weak var text_verify: UITextField!
    @IBOutlet weak var view_verify: UIView!
    var verificationCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.text_verify.delegate = self
        self.text_verify.text = verificationCode
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.image_red1.isHidden = true
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            
            self.view_verify.semanticContentAttribute = .forceLeftToRight
            self.superView.semanticContentAttribute = .forceLeftToRight
            self.label_verify.text =  ("Enter the verification code sent on your registered mobile number.".localizeStringUsingSystemLang)
            self.label_verify.textAlignment = .left
            self.text_verify.attributedPlaceholder = NSAttributedString(string: "Verification Code".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.button_verify.setTitle("Verify".localizeStringUsingSystemLang, for: .normal)
        }
        else if(language == "دری"){
            
            self.view_verify.semanticContentAttribute = .forceRightToLeft
            self.superView.semanticContentAttribute = .forceRightToLeft
             self.label_verify.text =  ("Enter the verification code sent on your registered mobile number.".localizeString1(lang: "fa-AF"))
             self.label_verify.textAlignment = .right
            self.text_verify.attributedPlaceholder = NSAttributedString(string: "Verification Code".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.button_verify.setTitle("Verify".localizeString1(lang: "fa-AF"), for: .normal)
        }
        else {
           
            self.view_verify.semanticContentAttribute = .forceRightToLeft
            self.superView.semanticContentAttribute = .forceRightToLeft
             self.label_verify.text =  ("Enter the verification code sent on your registered mobile number.".localizeString1(lang: "ps-AF"))
             self.label_verify.textAlignment = .right
            self.text_verify.attributedPlaceholder = NSAttributedString(string: "Verification Code".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.button_verify.setTitle("Verify".localizeString1(lang: "ps-AF"), for: .normal)
        }
    }

    @IBAction func action_verify(_ sender: Any) {
        
        if(self.text_verify.text == "" ){
            
            self.image_red1.isHidden = true
        }
        else {
            
            if CheckConnection.isConnectedToNetwork() {
                print("Connected")
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = .indeterminate
                loadingNotification.label.text = "Loading"
                
                
                Alamofire.request("http://qtademo.com/saraf/api/m_varify", method: .post, parameters: ["m":UserDefaults.standard.value(forKey: "MobileNo")! , "vc" : self.text_verify.text!], encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    switch response.result {
                        
                        
                    case .success:
                        
                        
                        print(response)
                        
                        let json : [String : Any] = response.result.value as! [String : Any]
                        let code :Int = json["code"] as! Int
                        
                        if(code == 200){
                            let data = json["data"] as! NSDictionary
                            let authrtoken = json["auth_token"] as! String
                            
                            print("auth token = \(authrtoken)")
                            UserDefaults.standard.set(authrtoken, forKey: "auth_token")
                            
                            UserDefaults.standard.set("user", forKey: "LoginType")
                        //    UserDefaults.standard.removeObject(forKey: "MobileNo")
                            
                            let regisetr : tabbarController = self.storyboard?.instantiateViewController(withIdentifier: "tabbarController") as! tabbarController
                            self.navigationController?.pushViewController(regisetr, animated: true)
                        }
                        
                        
                        break
                    case .failure(let error):
                        
                        print(error)
                        let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(actionSheet)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                })
                
            }
            else{
                let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
                let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                })
                alert.addAction(actionSheet)
                self.present(alert, animated: true, completion: nil)
            }
            
            
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

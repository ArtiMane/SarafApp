//
//  AddPost.swift
//  localization
//
//  Created by Developer on 5/13/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class AddPost: UIViewController , UIPickerViewDataSource , UITextFieldDelegate ,UIPickerViewDelegate  , UITextViewDelegate{

    
    @IBOutlet weak var upperView: UIView!
    
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var label_myPost: UILabel!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var button_addPost: UIButton!
    @IBOutlet weak var image_red3: UIImageView!
    @IBOutlet weak var image_red2: UIImageView!
    @IBOutlet weak var image_red1: UIImageView!
    @IBOutlet weak var text_desc: UITextView!
    @IBOutlet weak var text_amount: UITextField!
    @IBOutlet weak var text_from: UITextField!
    @IBOutlet weak var text_to: UITextField!
    @IBOutlet weak var view_desc: UIView!
    @IBOutlet weak var label_name: UILabel!
    
    @IBOutlet weak var image_red8: UIImageView!
    @IBOutlet weak var image_red5: UIImageView!
    @IBOutlet weak var image_red4: UIImageView!
    @IBOutlet weak var button_addpost: UIButton!
    @IBOutlet weak var view_from: UIView!
    
    @IBOutlet weak var view_to: UIView!
    
    @IBOutlet weak var text_market: UITextField!
    @IBOutlet weak var view_city: UIView!
    
    @IBOutlet weak var text_city: UITextField!
    @IBOutlet weak var view_market: UIView!
    
    @IBOutlet weak var view_amount: UIView!
    
    var pickerView = UIPickerView()
    var pickerView1 = UIPickerView()
    var pickerView_city = UIPickerView()
    var pickerView_market = UIPickerView()
    var arrayLanguage = [String]()
    var arraySelectedMarketId = [String]()
    var selectedMarketId = ""
    var selectedMarketId1 = ""
    
    var arrayCity = [String]()
    var arraySelectedCity = [String]()
    var selectedCityId = ""
    
    var arrayMarket = [String]()
    var arraySelectedMarket = [String]()
    var selectedMarket = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        self.pickerView1.dataSource = self
        self.pickerView1.delegate = self
        self.pickerView_city.dataSource = self
        self.pickerView_city.delegate = self
        self.pickerView_market.dataSource = self
        self.pickerView_market.delegate = self
        
        self.pickerView1.delegate = self
        self.text_to.inputView = pickerView
        self.text_from.inputView = pickerView1
        self.text_city.inputView = pickerView_city
        self.text_market.inputView = pickerView_market
        
        
        
        self.view_to.layer.cornerRadius = 3.0
        self.view_desc.layer.cornerRadius = 3.0
        self.view_from.layer.cornerRadius = 3.0
        self.view_amount.layer.cornerRadius = 3.0
        self.view_market.layer.cornerRadius = 3.0
        self.view_city.layer.cornerRadius = 3.0
        self.button_addPost.layer.cornerRadius = 3.0
        
        self.text_amount.keyboardType = .numberPad
        self.text_amount.delegate = self
        
        self.callCities()
        
    self.label_name.text  = (UserDefaults.standard.value(forKey: "fname") as! String) + " " + (UserDefaults.standard.value(forKey: "lname") as! String)
        
        self.text_desc.delegate = self
        
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            self.text_desc.text = "Please enter the description".localizeStringUsingSystemLang
        }
        else if(language == "دری"){
          self.text_desc.text = "Please enter the description".localizeString1(lang: "fa-AF")
        }
        else {
           self.text_desc.text = "Please enter the description".localizeString1(lang: "ps-AF")
        }
        
        self.text_desc.textColor = UIColor.lightGray
        
        self.getCurrencies()
    }
    
    
    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == self.text_amount){
        self.image_red3.isHidden = true
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            
            
            self.superView.semanticContentAttribute = .forceLeftToRight
            self.view_to.semanticContentAttribute = .forceLeftToRight
            self.view_desc.semanticContentAttribute = .forceLeftToRight
            self.view_from.semanticContentAttribute = .forceLeftToRight
            self.view_amount.semanticContentAttribute = .forceLeftToRight
            self.view_city.semanticContentAttribute = .forceLeftToRight
            self.view_market.semanticContentAttribute = .forceLeftToRight
            self.upperView.semanticContentAttribute = .forceLeftToRight
            
            self.text_to.attributedPlaceholder = NSAttributedString(string: "To Currency".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_from.attributedPlaceholder = NSAttributedString(string: "From Currency".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_amount.attributedPlaceholder = NSAttributedString(string: "Amount".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_city.attributedPlaceholder = NSAttributedString(string: "City".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_market.attributedPlaceholder = NSAttributedString(string: "Market".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            self.label_name.textAlignment = .left
            self.text_from.textAlignment = .left
            self.text_desc.textAlignment = .left
            self.text_amount.textAlignment = .left
            self.text_to.textAlignment = .left
            self.text_market.textAlignment = .left
            self.text_city.textAlignment = .left
            
            
            self.label_myPost.text = ("Add Post".localizeStringUsingSystemLang)
            
            self.button_addPost.setTitle("Add Post".localizeStringUsingSystemLang, for: .normal)
            
            self.buttonBack.setImage(UIImage(named : "leftArrow.png"), for: .normal)
           // self.label_sun.text = ("SUN".localizeStringUsingSystemLang)
        }
        else if(language == "دری"){
            self.superView.semanticContentAttribute = .forceRightToLeft
            self.view_to.semanticContentAttribute = .forceRightToLeft
            self.view_desc.semanticContentAttribute = .forceRightToLeft
            self.view_from.semanticContentAttribute = .forceRightToLeft
            self.view_amount.semanticContentAttribute = .forceRightToLeft
            self.upperView.semanticContentAttribute = .forceRightToLeft
            self.view_city.semanticContentAttribute = .forceRightToLeft
            self.view_market.semanticContentAttribute = .forceRightToLeft
            
            self.text_to.attributedPlaceholder = NSAttributedString(string: "To Currency".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_from.attributedPlaceholder = NSAttributedString(string: "From Currency".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_amount.attributedPlaceholder = NSAttributedString(string: "Amount".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_city.attributedPlaceholder = NSAttributedString(string: "City".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_market.attributedPlaceholder = NSAttributedString(string: "Market".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
             self.label_myPost.text = ("Add Post".localizeString1(lang: "fa-AF"))
            
            self.label_name.textAlignment = .right
            self.text_from.textAlignment = .right
            self.text_desc.textAlignment = .right
            self.text_amount.textAlignment = .right
            self.text_to.textAlignment = .right
            self.text_market.textAlignment = .right
            self.text_city.textAlignment = .right
           // self.label_sun.text = ("SUN".localizeString1(lang: "fa-AF"))
            
            self.button_addPost.setTitle("Add Post".localizeString1(lang: "fa-AF"), for: .normal)
            
            self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
        }
        else {
            self.superView.semanticContentAttribute = .forceRightToLeft
            self.view_to.semanticContentAttribute = .forceRightToLeft
            self.view_desc.semanticContentAttribute = .forceRightToLeft
            self.view_from.semanticContentAttribute = .forceRightToLeft
            self.view_amount.semanticContentAttribute = .forceRightToLeft
            self.upperView.semanticContentAttribute = .forceRightToLeft
            self.view_city.semanticContentAttribute = .forceRightToLeft
            self.view_market.semanticContentAttribute = .forceRightToLeft
            
            self.text_to.attributedPlaceholder = NSAttributedString(string: "To Currency".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_from.attributedPlaceholder = NSAttributedString(string: "From Currency".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_amount.attributedPlaceholder = NSAttributedString(string: "Amount".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_city.attributedPlaceholder = NSAttributedString(string: "City".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_market.attributedPlaceholder = NSAttributedString(string: "Market".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
             self.label_myPost.text = ("Add Post".localizeString1(lang: "ps-AF"))
            
            self.label_name.textAlignment = .right
            self.text_from.textAlignment = .right
            self.text_desc.textAlignment = .right
            self.text_amount.textAlignment = .right
            self.text_to.textAlignment = .right
            self.text_market.textAlignment = .right
            self.text_city.textAlignment = .right
            
            self.button_addPost.setTitle("Add Post".localizeString1(lang: "ps-AF"), for: .normal)
            
            self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
           // self.label_sun.text = ("SUN".localizeString1(lang: "ps-AF"))
        }
    }
    
    
    @IBAction func button_addPost(_ sender: Any) {
        
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            loadingNotification.label.text = "Loading..."
            
            
            Alamofire.request("http://qtademo.com/saraf/api/currencies_list", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let arrayLanguage : NSArray = json["cur_list"] as! NSArray
                    
                    for var i in 0..<arrayLanguage.count{
                        
                        let language = (arrayLanguage[i] as! NSDictionary).value(forKey: "cur_code")
                        self.arrayLanguage.append(language as! String)
                        
                        let languageId = (arrayLanguage[i] as! NSDictionary).value(forKey: "cur_id")
                        self.arraySelectedMarketId.append(languageId as! String)
                    }
                    self.pickerView.reloadAllComponents()
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func getCurrencies(){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            loadingNotification.label.text = "Loading..."
            
            
            Alamofire.request("http://qtademo.com/saraf/api/currencies_list", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let arrayLanguage : NSArray = json["cur_list"] as! NSArray
                    
                    for var i in 0..<arrayLanguage.count{
                        
                        let language = (arrayLanguage[i] as! NSDictionary).value(forKey: "cur_code")
                        self.arrayLanguage.append(language as! String)
                        
                        let languageId = (arrayLanguage[i] as! NSDictionary).value(forKey: "cur_id")
                        self.arraySelectedMarketId.append(languageId as! String)
                    }
                    self.pickerView.reloadAllComponents()
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    //MARK: - Fetch cities
    
    func callCities(){
        
        
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
          //  loadingNotification.label.text = "Fetching Cities"
            
            
            Alamofire.request("http://qtademo.com/saraf/api/cities", method: .post, parameters: ["c_id" : UserDefaults.standard.value(forKey: "c_id")!], encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let arrayLanguage : NSArray = json["cities"] as! NSArray
                    print(json)
                    for var i in 0..<arrayLanguage.count{
                        
                        let language = (arrayLanguage[i] as! NSDictionary).value(forKey: "ct_name")
                        self.arrayCity.append(language as! String)
                        
                        let languageId = (arrayLanguage[i] as! NSDictionary).value(forKey: "ct_id")
                        self.arraySelectedCity.append(languageId as! String)
                        
                        
                    }
                    self.pickerView_city.reloadAllComponents()
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func callMarket(){
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            // loadingNotification.label.text = "Fetching countries"
            
            
            Alamofire.request("http://qtademo.com/saraf/api/exm_list", method: .post, parameters: ["ct_id" : selectedCityId], encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let arrayLanguage : NSArray = json["exm_list"] as! NSArray
                    
                    for var i in 0..<arrayLanguage.count{
                        
                        let language = (arrayLanguage[i] as! NSDictionary).value(forKey: "exm_name")
                        self.arrayMarket.append(language as! String)
                        
                        let id1 = (arrayLanguage[i] as! NSDictionary).value(forKey: "exm_id")
                        self.arraySelectedMarket.append(id1 as! String)
                        
                        
                        
                        
                    }
                    self.pickerView_market.reloadAllComponents()
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    //MARK: - Pickerview method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == self.pickerView || pickerView == self.pickerView1 ){
             return arrayLanguage.count
        }
        else if(pickerView == self.pickerView_city ){
            return arrayCity.count
        }
        else {
             return arrayMarket.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == self.pickerView || pickerView == self.pickerView1 ){
            return arrayLanguage[row]
        }
        else if(pickerView == self.pickerView_city ){
            return arrayCity[row]
        }
        else {
            return arrayMarket[row]
        }
       
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == self.pickerView){
            
            self.image_red1.isHidden = true
            self.text_to.text = arrayLanguage[row]
            selectedMarketId = arraySelectedMarketId[row]
        }
        else if(pickerView == self.pickerView1){
            self.image_red2.isHidden = true
            self.text_from.text = arrayLanguage[row]
            selectedMarketId1 = arraySelectedMarketId[row]
        }
        else if(pickerView == self.pickerView_city){
            self.image_red8.isHidden = true
            self.text_city.text = arrayCity[row]
            selectedCityId = arraySelectedCity[row]
            self.callMarket()
        }
        else if(pickerView == self.pickerView_market){
            self.image_red5.isHidden = true
            self.text_market.text = arrayMarket[row]
            selectedMarket = arraySelectedMarket[row]
        }
        
       
        
    }
    
    func checkValidation() -> Bool {
        
        let myString = self.text_desc.text!
        var myString1 = ""
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            myString1 = "Please enter the description".localizeStringUsingSystemLang
        }
        else if(language == "دری"){
            myString1 = "Please enter the description".localizeString1(lang: "fa-AF")
        }
        else {
            myString1 = "Please enter the description".localizeString1(lang: "ps-AF")
        }
        if(myString == myString1){
            
        }
        else {
            let test = String(describing: myString.filter { !"\n\t\r".contains($0) })
            self.text_desc.text = test
        }
        
        
        if(self.text_desc.text == ""){
            if let selectedRange = self.text_desc.selectedTextRange {
                
                let cursorPosition = self.text_desc.offset(from: self.text_desc.beginningOfDocument, to: selectedRange.start)
                
                print("\(cursorPosition)")
            }
            
            let newPosition = self.text_desc.beginningOfDocument
            self.text_desc.selectedTextRange = self.text_desc.textRange(from: newPosition, to: newPosition)
        }
        
        var count = 0
        if(self.text_to.text == ""){
        
            self.image_red1.isHidden = false
            
    }
        else {
            self.image_red1.isHidden = true
            count = count + 1
        }
        
        if(self.text_desc.text == "" || self.text_desc.text == myString1 ){
            
            self.image_red4.isHidden = false
            
        }
        else {
            self.image_red4.isHidden = true
            count = count + 1
        }
        
        if(self.text_from.text == ""){
            
            self.image_red2.isHidden = false
            
        }
        else {
            self.image_red2.isHidden = true
            count = count + 1
        }
        
        if(self.text_amount.text == ""){
            
            self.image_red3.isHidden = false
            
        }
        else {
            self.image_red3.isHidden = true
            count = count + 1
        }
        
        if(count == 4){
            return true
        }
        else {
            return false
        }
    }
    
    
    @IBAction func action_addPost(_ sender: Any) {
        
        if(self.checkValidation() == false){
            
        }
        else {
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            let auth_token = UserDefaults.standard.value(forKey: "auth_token") as! String
            print(auth_token)
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            Alamofire.request("http://qtademo.com/saraf/api/new_post", method: .post , parameters: ["uid" : UserDefaults.standard.value(forKey: "uid")! , "ct_id" : UserDefaults.standard.value(forKey: "ct_id")! ,"exm_id" : UserDefaults.standard.value(forKey: "exm_id")! ,"buy_cur_id" : selectedMarketId ,"sell_cur_id" : selectedMarketId1 ,"amount" : self.text_amount.text! ,"comment" : self.text_desc.text!  ], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        
                        let alert : UIAlertController = UIAlertController(title: "Error", message: "Error while adding new post.", preferredStyle: .actionSheet)
                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(actionSheet)
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.image_red4.isHidden = true
        if self.text_desc.textColor == UIColor.lightGray {
            self.text_desc.text = ""
            self.text_desc.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if self.text_desc.text == "" {
            
            let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
            if(language == "English"){
                self.text_desc.text = "Please enter the description".localizeStringUsingSystemLang
            }
            else if(language == "دری"){
                self.text_desc.text = "Please enter the description".localizeString1(lang: "fa-AF")
            }
            else {
                self.text_desc.text = "Please enter the description".localizeString1(lang: "ps-AF")
            }
            self.text_desc.textColor = UIColor.lightGray
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

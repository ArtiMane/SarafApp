//
//  PostVC.swift
//  localization
//
//  Created by Developer on 5/13/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class PostVC: UIViewController, UITableViewDataSource , UITableViewDelegate {

    @IBOutlet weak var label_usDate: UILabel!
    @IBOutlet weak var label_afghanDate: UILabel!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var label_sun: UILabel!
    @IBOutlet weak var label_allPosts: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var myScrollView: UIScrollView!
     var segmnetedControl1  = UISegmentedControl()
    var exmidArray = NSMutableArray()
    var arrayMarket = NSMutableArray()
    var selectedSegment = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.segmnetedControl1.frame = CGRect(x: 8, y: 8, width: 0, height: 40)
        self.segmnetedControl1.tintColor = UIColor(displayP3Red: 0/255, green: 209/255, blue: 174/255, alpha: 1.0)
        self.segmnetedControl1.addTarget(self, action: #selector(PostVC.segmentedSelected(sender:)), for: .valueChanged)
        self.myScrollView.addSubview(segmnetedControl1)
        
        let date = Date()
       
        let mystring = ""
        self.label_afghanDate.text = mystring.getAfghanDate(date: date)
        self.label_sun.text = mystring.getDay(date: date)
        self.label_usDate.text = mystring.getUSDate(date: date)
        
        self.callExchangeMarketList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            self.tableView.semanticContentAttribute = .forceLeftToRight
            self.superView.semanticContentAttribute = .forceLeftToRight
            
            self.label_allPosts.text = ("All Posts".localizeStringUsingSystemLang)
           // self.label_sun.text = ("SUN".localizeStringUsingSystemLang)
        }
        else if(language == "دری"){
            self.tableView.semanticContentAttribute = .forceRightToLeft
            self.superView.semanticContentAttribute = .forceRightToLeft
            
            self.label_allPosts.text = ("All Posts".localizeString1(lang: "fa-AF"))
           // self.label_sun.text = ("SUN".localizeString1(lang: "fa-AF"))
        }
        else {
            self.tableView.semanticContentAttribute = .forceRightToLeft
            self.superView.semanticContentAttribute = .forceRightToLeft
            
            self.label_allPosts.text = ("All Posts".localizeString1(lang: "ps-AF"))
            //self.label_sun.text = ("SUN".localizeString1(lang: "ps-AF"))
        }
        
        if(UserDefaults.standard.value(forKey: "valueUpdated") != nil){
            
            let value = UserDefaults.standard.value(forKey: "valueUpdated") as! String
            if(value == "1"){
                
                self.callExchangeMarketList()
                
            }
            UserDefaults.standard.set("0", forKey: "valueUpdated")
        }
    }
    
    func callExchangeMarketList(){
        
        let usertype = UserDefaults.standard.value(forKey: "LoginType") as! String
        var agent = ""
        if(usertype == "user"){
            agent  = "0"
        }
        else {
            agent = "1"
        }
        
        print("agent value = \(agent)")
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            let auth_token = UserDefaults.standard.value(forKey: "auth_token") as! String
            print(auth_token)
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            Alamofire.request("http://qtademo.com/saraf/api/exchange_market_list", method: .post , parameters: ["agent" : agent], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        let data1 = json["data"] as! NSArray
                        let exm_id = UserDefaults.standard.value(forKey: "exm_id") as! String
                        
                        let counnt = data1.count
                        
                        for var i in 0 ..< data1.count {
                            
                            let exm_id1 = (data1[i] as! NSDictionary).value(forKey: "exm_id") as! String
                            var exm_name = ""
                            let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
                            if(language == "English"){
                                exm_name =  (data1[i] as! NSDictionary).value(forKey: "exm_name") as! String
                            }
                            else if(language == "دری"){
                                exm_name =  (data1[i] as! NSDictionary).value(forKey: "exm_dari_name") as! String
                            }
                            else {
                                exm_name =  (data1[i] as! NSDictionary).value(forKey: "exm_pashtu_name") as! String
                            }
                          //  let exm_name = (data1[i] as! NSDictionary).value(forKey: "exm_name") as! String
                            self.exmidArray.add(exm_id1)
                            self.segmnetedControl1.insertSegment(withTitle: exm_name, at: i, animated: true)
                            self.segmnetedControl1.apportionsSegmentWidthsByContent = true
                            self.segmnetedControl1.setWidth(200, forSegmentAt: i)
                            
                        }
                        self.segmnetedControl1.apportionsSegmentWidthsByContent = true
                        
                        let totalwidth = counnt * 200
                        self.segmnetedControl1.frame = CGRect(x: self.segmnetedControl1.frame.origin.x, y: self.segmnetedControl1.frame.origin.y, width: CGFloat(totalwidth), height: self.segmnetedControl1.frame.size.height)
                        self.myScrollView.contentSize = CGSize(width: self.segmnetedControl1.frame.size.width + 8, height: self.myScrollView.frame.size.height)
                        self.segmnetedControl1.selectedSegmentIndex = 0
                        self.callMarketListExm(exmId: self.exmidArray.object(at: 0) as! String)
                        
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    @objc func segmentedSelected(sender : UISegmentedControl){
        
        selectedSegment = sender.selectedSegmentIndex
        UserDefaults.standard.set(selectedSegment, forKey: "selectedSegment")
        let selected = sender.selectedSegmentIndex
        self.arrayMarket.removeAllObjects()
        self.tableView.reloadData()
        self.callMarketListExm(exmId: exmidArray.object(at: selected) as! String)
        
        
        
    }
    func callMarketListExm(exmId : String){
        
        let usertype = UserDefaults.standard.value(forKey: "LoginType") as! String
        var agent = ""
        if(usertype == "user"){
            agent  = "0"
        }
        else {
            agent = "1"
        }
        
        print("agent value = \(agent)")
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            //    loadingNotification.label.text = "Registering"
            
            let headers : HTTPHeaders = [
                "auth_token": UserDefaults.standard.value(forKey: "auth_token") as! String
            ]
            
            Alamofire.request("http://qtademo.com/saraf/api/post_list", method: .post , parameters: ["exm_id" : exmId , "agent" : agent], encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                         if( json["data"] as? NSDictionary != nil){
                            
                            if((json["data"] as! NSDictionary).value(forKey: "data") as? NSArray != nil){
                            self.arrayMarket = ((json["data"] as! NSDictionary).value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                                 self.tableView.reloadData()
                            }
                       
                        }
                         else {
                             self.tableView.reloadData()
                        }
                    }
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMarket.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PostCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! PostCell
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            cell.superView.semanticContentAttribute = .forceLeftToRight
            cell.label_name.semanticContentAttribute = .forceLeftToRight
            cell.label_date.semanticContentAttribute = .forceLeftToRight
            cell.label_name.textAlignment = .left
            cell.label_date.textAlignment = .left
            cell.label_to1.text = ("To".localizeStringUsingSystemLang)
            cell.label_sell.text = ("SELL".localizeStringUsingSystemLang)
            cell.viewuppwe.semanticContentAttribute = .forceLeftToRight
            cell.label_sun.textAlignment = .left
        }
        else if(language == "دری"){
            cell.superView.semanticContentAttribute = .forceRightToLeft
            cell.label_name.semanticContentAttribute = .forceRightToLeft
            cell.label_date.semanticContentAttribute = .forceRightToLeft
            cell.label_to1.text = ("To".localizeString1(lang: "fa-AF"))
             cell.label_sell.text = ("SELL".localizeString1(lang: "fa-AF"))
            cell.label_name.textAlignment = .right
            cell.label_date.textAlignment = .right
            cell.viewuppwe.semanticContentAttribute = .forceRightToLeft
            cell.label_sun.textAlignment = .right
        }
        else {
            cell.superView.semanticContentAttribute = .forceRightToLeft
            cell.label_name.semanticContentAttribute = .forceRightToLeft
            cell.label_date.semanticContentAttribute = .forceRightToLeft
            cell.label_name.textAlignment = .right
            cell.label_date.textAlignment = .right
           
            cell.label_to1.text = ("To".localizeString1(lang: "ps-AF"))
             cell.label_sell.text = ("SELL".localizeString1(lang: "ps-AF"))
            cell.viewuppwe.semanticContentAttribute = .forceRightToLeft
            cell.label_sun.textAlignment = .right
        }
        
        
        
        
        cell.label_name.text = ((arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "fname") as! String) + " " +  ((arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "lname") as! String)
        
        cell.label_to.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "buy_cur") as? String
        cell.label_from.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "sell_cur") as? String
        
        let amount = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "amount") as! String
        cell.label_amount.text =  String(describing: Double(amount)!.rounded(toPlaces: 2))
       // cell.label_amount.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "amount") as? String
//        cell.label_desc.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "comment") as? String
        
        let unixTimestamp = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "created_datetime") as! String
        let date = Date(timeIntervalSince1970: Double(unixTimestamp)!)
        
        let myString = ""
        cell.label_sun.text = (myString.getDay(date: date)) + " : " + (myString.getAfghanDate(date: date)) + " / " + (myString.getUSDate(date: date))
        
        cell.superView.layer.cornerRadius = 3.0
        let dateFormatter = DateFormatter()
        // dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "hh:mm aa" //Specify your format that you want
        cell.label_date.text = dateFormatter.string(from: date)
        
//        cell.label_date.text = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "to_cur_code") as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 153.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let regisetr : PostDetails = self.storyboard?.instantiateViewController(withIdentifier: "PostDetails") as! PostDetails
        regisetr.exp_id = (arrayMarket.object(at: indexPath.row) as! NSDictionary).value(forKey: "exp_id") as! String
        self.navigationController?.pushViewController(regisetr, animated: true)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

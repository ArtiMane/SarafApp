//
//  RegisterAdminVC.swift
//  localization
//
//  Created by Developer on 4/30/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class RegisterAdminVC: UIViewController {

    @IBOutlet weak var viewTitle: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var buttonRegister: UIButton!
    @IBOutlet weak var textMobile: UITextField!
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textUserName: UITextField!
    @IBOutlet weak var textType: UITextField!
    @IBOutlet weak var textLastName: UITextField!
    @IBOutlet weak var textFirstName: UITextField!
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var viewFirstName: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        self.viewType.layer.cornerRadius = 3.0
        self.viewEmail.layer.cornerRadius = 3.0
        self.viewPassword.layer.cornerRadius = 3.0
        self.viewMobile.layer.cornerRadius = 3.0
        self.viewTitle.layer.cornerRadius = 3.0
        self.viewLastName.layer.cornerRadius = 3.0
        self.viewUserName.layer.cornerRadius = 3.0
        self.viewFirstName.layer.cornerRadius = 3.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(UserDefaults.standard.value(forKey: "selectedLanguage") as! String != "English"){
            
            self.viewType.semanticContentAttribute = .forceRightToLeft
            self.viewEmail.semanticContentAttribute = .forceRightToLeft
            self.viewPassword.semanticContentAttribute = .forceRightToLeft
            self.viewMobile.semanticContentAttribute = .forceRightToLeft
            self.viewTitle.semanticContentAttribute = .forceRightToLeft
            self.viewLastName.semanticContentAttribute = .forceRightToLeft
            self.viewUserName.semanticContentAttribute = .forceRightToLeft
            self.viewFirstName.semanticContentAttribute = .forceRightToLeft
            
            self.textMobile.textAlignment  = .right
            self.textPassword.textAlignment = .right
            self.textType.textAlignment = .right
            self.textEmail.textAlignment = .right
            self.textLastName.textAlignment = .right
            self.textFirstName.textAlignment = .right
            self.textUserName.textAlignment = .right
            
            
           
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
    }

    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func action_register(_ sender: Any) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

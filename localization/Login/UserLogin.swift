//
//  UserLogin.swift
//  localization
//
//  Created by Developer on 5/13/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class UserLogin: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var superView: UIView!
    
    @IBOutlet weak var text_mobile: UITextField!
    @IBOutlet weak var image_red2: UIImageView!
    @IBOutlet weak var image_red1: UIImageView!
    @IBOutlet weak var view_password: UIView!
    @IBOutlet weak var button_signIn: UIButton!
    @IBOutlet weak var view_mobile: UIView!
    @IBOutlet weak var label_signIn: UILabel!
    @IBOutlet weak var buttonBack: UIButton!
    
    @IBOutlet weak var button_agentLogin: UIButton!
    @IBOutlet weak var text_password: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.text_mobile.text = "9420429240"
        self.text_password.text = "123456"
        
        self.text_password.delegate = self
        self.text_mobile.delegate = self
        
        self.view_mobile.layer.cornerRadius = 3.0
        self.view_password.layer.cornerRadius = 3.0
        self.button_signIn.layer.cornerRadius = 3.0
        
        self.text_mobile.keyboardType = .numberPad
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == self.text_password){
            self.image_red2.isHidden = true
        }
        if(textField == self.text_mobile){
            self.image_red1.isHidden = true
        }
        return true
    }
    
    @IBAction func action_agentLogin(_ sender: Any) {
        
        let regisetr : AgentLogin = self.storyboard?.instantiateViewController(withIdentifier: "AgentLogin") as! AgentLogin
        self.navigationController?.pushViewController(regisetr, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        let language : String = UserDefaults.standard.value(forKey: "AppLanguageKey") as! String
        if(language == "English"){
            self.superView.semanticContentAttribute = .forceLeftToRight
            self.view_mobile.semanticContentAttribute = .forceLeftToRight
            self.view_password.semanticContentAttribute = .forceLeftToRight
            
            self.text_mobile.textAlignment = .left
            self.text_password.textAlignment = .left
            self.label_signIn.textAlignment = .left
            
            self.text_password.attributedPlaceholder = NSAttributedString(string: "Password".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_mobile.attributedPlaceholder = NSAttributedString(string: "Mobile No".localizeStringUsingSystemLang,attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            self.label_signIn.text = ("Sign In".localizeStringUsingSystemLang)
            
            self.buttonBack.setImage(UIImage(named : "leftArrow.png"), for: .normal)
            
            self.button_agentLogin.setTitle("Agent Login".localizeStringUsingSystemLang, for: .normal)
            
             self.button_signIn.setTitle("Sign In".localizeStringUsingSystemLang, for: .normal)
            
            
        }
        else if(language == "دری"){
            self.superView.semanticContentAttribute = .forceRightToLeft
            self.view_mobile.semanticContentAttribute = .forceRightToLeft
            self.view_password.semanticContentAttribute = .forceRightToLeft
            
            self.text_mobile.textAlignment = .right
            self.text_password.textAlignment = .right
            self.label_signIn.textAlignment = .right
            
            self.text_password.attributedPlaceholder = NSAttributedString(string: "Password".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_mobile.attributedPlaceholder = NSAttributedString(string: "Mobile No".localizeString1(lang: "fa-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            self.label_signIn.text = ("Sign In".localizeString1(lang: "fa-AF"))
            
             self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
            
            self.button_agentLogin.setTitle("Agent Login".localizeString1(lang: "fa-AF"), for: .normal)
            
             self.button_signIn.setTitle("Sign In".localizeString1(lang: "fa-AF"), for: .normal)
        }
        else {
            self.superView.semanticContentAttribute = .forceRightToLeft
            self.view_mobile.semanticContentAttribute = .forceRightToLeft
            self.view_password.semanticContentAttribute = .forceRightToLeft
            
            self.text_mobile.textAlignment = .right
            self.text_password.textAlignment = .right
            self.label_signIn.textAlignment = .right
            
            self.text_password.attributedPlaceholder = NSAttributedString(string: "Password".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            self.text_mobile.attributedPlaceholder = NSAttributedString(string: "Mobile No".localizeString1(lang: "ps-AF"),attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 0.75, alpha: 1.0)])
            
            self.label_signIn.text = ("Sign In".localizeString1(lang: "ps-AF"))
            
             self.buttonBack.setImage(UIImage(named : "rightArrow.png"), for: .normal)
            
             self.button_agentLogin.setTitle("Agent Login".localizeString1(lang: "ps-AF"), for: .normal)
            
             self.button_signIn.setTitle("Sign In".localizeString1(lang: "ps-AF"), for: .normal)
        }
    }
    
    
    @IBAction func action_signIn(_ sender: Any) {
        
//        let headers : HTTPHeaders = [
//            "Content-Type": "application/x-www-form-urlencoded",
//            "Accept": "application/xml"
//        ]
//
//        Alamofire.request("https://login.veevavault.com/api/v18.1/auth", method: .post, parameters: ["username":"krishnan.varagur@sunpharma.com.fs" , "password" : "OSG!SP#Y2K18" ], encoding: URLEncoding.default, headers: headers).responseString(completionHandler: { (response) in
//
//            print(response)
//        })
        
        
      
        if(self.text_password.text == "" &&  self.text_mobile.text == ""){
            
            image_red2.isHidden = false
            image_red1.isHidden = false
        }
        else if(self.text_mobile.text == ""){
            
            image_red1.isHidden = false
        }
        else if(self.text_password.text == ""){
            
            image_red2.isHidden = false
        }
        else {
        
        if CheckConnection.isConnectedToNetwork() {
            print("Connected")
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = .indeterminate
            loadingNotification.label.text = "Loading"
            
            
            Alamofire.request("http://qtademo.com/saraf/api/user_login", method: .post, parameters: ["m":self.text_mobile.text! , "p" : self.text_password.text! ], encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                switch response.result {
                    
                    
                case .success:
                    
                    
                    print(response)
                    
                    let json : [String : Any] = response.result.value as! [String : Any]
                    let code :Int = json["code"] as! Int
                    
                    if(code == 200){
                        let data = json["data"] as! NSDictionary
                        let authrtoken = json["auth_token"] as! String
                        let exm_id = data["exm_id"] as! String
                        print("auth token = \(authrtoken)")
                        UserDefaults.standard.set(authrtoken, forKey: "auth_token")
                        UserDefaults.standard.set(exm_id, forKey: "exm_id")
                        UserDefaults.standard.set(data["uid"], forKey: "uid")
                        UserDefaults.standard.set(data["ct_id"], forKey: "ct_id")
                         UserDefaults.standard.set(data["c_id"], forKey: "c_id")
                        
                        UserDefaults.standard.setValue(self.text_mobile.text, forKey: "MobileNo")
                        UserDefaults.standard.setValue(self.text_password.text, forKey: "Password")
                        
                        UserDefaults.standard.set(data["fname"], forKey: "fname")
                        UserDefaults.standard.set(data["lname"], forKey: "lname")
                        
                        let lid = data["lid"] as! String
                        var selectedLanguageId = ""
                        if(lid == "1"){
                            selectedLanguageId = "English"
                        }
                        if(lid == "2"){
                            selectedLanguageId = "دری"
                        }
                        if(lid == "3"){
                            selectedLanguageId = "pasto"
                        }
                      //  UserDefaults.standard.set(selectedLanguageId, forKey: "AppLanguageKey")
                    let email = data["email"] as! String
                       UserDefaults.standard.set("user", forKey: "LoginType")

                        
                        
                        User.loginUser(withEmail: email, password: self.text_password.text!, completion: { [weak weakSelf = self] (status) in
                            DispatchQueue.main.async {
                                if status == true {
                                 //   weakSelf?.pushTo(viewController: .conversations)
                                } else {
                                   // weakSelf?.pushTo(viewController: .welcome)
                                }
                              //  weakSelf = nil
                            }
                        })
 
                        /*
                        User.registerUser(withFname: "John", lname: "smith" , mobile: "9890435520", sarafi_name: "abc124", uid: UserDefaults.standard.value(forKey: "uid") as! String, email: "John.snith@gmail.com", password: "123456", profilePic: UIImage(named:"profile.png")!, completion: { (status) in

                            MBProgressHUD.hide(for: self.view, animated: true)

                            if(status == true){

                                print("registered successfully")
                            }
                            else {
                                print("not registered")
                            }
                        })
 */
                    let regisetr : tabbarController = self.storyboard?.instantiateViewController(withIdentifier: "tabbarController") as! tabbarController
                    self.navigationController?.pushViewController(regisetr, animated: true)
                    }
                    else {
                        let alert : UIAlertController = UIAlertController(title: "Error", message: "Please enter valid credentials.", preferredStyle: .actionSheet)
                        let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(actionSheet)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                    
                    break
                case .failure(let error):
                    
                    print(error)
                    let alert : UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .actionSheet)
                    let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(actionSheet)
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Error", message: "Please check your network connection.", preferredStyle: .actionSheet)
            let actionSheet : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(actionSheet)
            self.present(alert, animated: true, completion: nil)
        }
       
        }
 
    }
    
    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

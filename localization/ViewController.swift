//
//  ViewController.swift
//  localization
//
//  Created by Developer on 4/24/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

let AppLanguageKey = "fa-AF"
let AppLanguageDefaultValue = "en"

var appLanguage: String {
    
    get {
        if let language = UserDefaults.standard.string(forKey: AppLanguageKey) {
            return language
        } else {
            UserDefaults.standard.setValue(AppLanguageDefaultValue, forKey: AppLanguageKey)
            return AppLanguageDefaultValue
        }
    }
    
    set(value) {
        UserDefaults.standard.setValue((value), forKey: AppLanguageKey)
    }
    
}


class ViewController: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var textType: UITextField!
    @IBOutlet weak var button_menu: UIButton!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var myView1: UIView!
    @IBOutlet weak var myView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // fa-AF
        
        if self.revealViewController() != nil {

            button_menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
//            button_menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        
    //    print("languages = \(languages)")
       // UserDefaults.standard.set("<#T##value: Any?##Any?#>", forKey: <#T##String#>)
        
        self.textType.delegate = self
    }
    @IBOutlet weak var mylabel: UILabel!
    
    
    override func viewDidAppear(_ animated: Bool) {
        let alertTitle = NSLocalizedString("Welcome", comment: "")
        let alertMessage = NSLocalizedString("Thank you for trying this app, you are a great person!", comment: "")
        let cancelButtonText = NSLocalizedString("Cancel", comment: "")
        let signupButtonText = NSLocalizedString("Signup", comment: "")
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: cancelButtonText, style: UIAlertActionStyle.cancel, handler: nil)
        let signupAction = UIAlertAction(title: signupButtonText, style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(signupAction)
        self.present(alert, animated: true, completion: nil)
        
        NSLog("title user lang: \("Welcome".localizeString)")
        NSLog("title En: \("Welcome".localizeString1(lang: "fa-AF"))")
//        NSLog("title Ru: \("Title".localizeString1(lang: "ru"))")
//        NSLog("title Fr: \("Title".localizeString1(lang: "fr"))")
//        NSLog("title ??: \("Title".localizeString1(lang: "blabla"))")
        NSLog("title sysem lnag: \("Welcome".localizeStringUsingSystemLang)")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
       
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func action_changeLanguage(_ sender: Any) {
        
      //  self.mylabel.semanticContentAttribute = .forceRightToLeft
      //  self.mylabel.textAlignment = .right
       // self.mylabel.semanticContentAttribute = .forceRightToLeft
        
      //  self.mylabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        
        
        
        if(appLanguage == AppLanguageDefaultValue){
            
            appLanguage = AppLanguageKey
            self.myView.semanticContentAttribute = .forceRightToLeft
            self.myView1.semanticContentAttribute = .forceRightToLeft
            
            self.label1.text = ("Welcome".localizeString)
            self.label2.text = ("Cancel".localizeString)
            self.label3.text = ("Signup".localizeString)
            
            self.label2.textAlignment  = .right
            self.label1.textAlignment  = .right
            self.label3.textAlignment  =  .right
            self.textType.textAlignment = .right
            
          //  self.revealViewController().revealToggle(animated: true)
            button_menu.removeTarget(nil, action: nil, for: .allEvents)
            button_menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
            
          //  self.revealViewController() = .right
//            if(UIApplication.shared.userInterfaceLayoutDirection == .leftToRight){
//
//               UIView.appearance().semanticContentAttribute = .forceRightToLeft
//            }
        }
        else {
            
            
            appLanguage = AppLanguageDefaultValue
            self.myView.semanticContentAttribute = .forceLeftToRight
            self.myView1.semanticContentAttribute = .forceLeftToRight
            
            self.label1.text = ("Welcome".localizeString)
            self.label2.text = ("Cancel".localizeString)
            self.label3.text = ("Signup".localizeString)
            
            self.label2.textAlignment  = .left
            self.label1.textAlignment  = .left
            self.label3.textAlignment  = .left
            self.textType.textAlignment = .left
            
          //  self.revealViewController().revealToggle(animated: true)
            button_menu.removeTarget(nil, action: nil, for: .allEvents)
            button_menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            
           // self.revealViewController().frontViewPosition = .left
            
//
//            if(UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft){
//
//                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            }
        }
        printMyLOcalization()
    }
    
    func printMyLOcalization(){
        
        NSLog("title user lang: \("Welcome".localizeString)")
        NSLog("title En: \("Welcome".localizeString1(lang: "fa-AF"))")
        NSLog("title sysem lnag: \("Welcome".localizeStringUsingSystemLang)")
    }
    
    

    
}

extension String {
    
    var localizeString: String {
        return localizeString1(lang: appLanguage)
    }
    
    var localizeStringUsingSystemLang: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func localizeString1(lang:String?) -> String {
        
        if let lang = lang {
            if let path = Bundle.main.path(forResource: lang, ofType: "lproj") {
                let bundle = Bundle(path: path)
                return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
            }
        }
        return localizeStringUsingSystemLang
    }
}

